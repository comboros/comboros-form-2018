const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');

const common = require('./webpack.common.js');

module.exports = merge(common, {
  mode: 'production',
  entry: {
    main: path.resolve(__dirname, 'src/static/index.js'),
    admin: path.resolve(__dirname, 'src/static/admin.js'),
  },
  module: {
    rules: [{
      test: /\.elm$/,
      include: [
        path.resolve(__dirname, 'src/Form')
      ],
      use: {
        loader: 'elm-webpack-loader',
        options: {
          cwd: path.resolve(__dirname),
          verbose: true,
          debug: false,
          optimize: true
        }
      }
    }, {
      test: /\.elm$/,
      include: [
        path.resolve(__dirname, 'src/Admin')
      ],
      use: {
        loader: 'elm-webpack-loader',
        options: {
          cwd: path.resolve(__dirname),
          verbose: true,
          debug: false,
          optimize: true
        }
      }
    }, {
      test: /\.sc?ss$/,
      use: [
        MiniCssExtractPlugin.loader,
        'css-loader',
        'sass-loader'
      ]
    }]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'static/css/[name]-[hash].css',
      chunkFilename: 'static/css/[id]-[hash].css',
    }),
    new CopyWebpackPlugin([{
      from: 'src/static/img/',
      to: 'static/img/'
    }, {
      from: 'src/static/img/favicon-96x96.png'
    }, {
      from: 'src/php/',
      to: 'api/'
    }])
  ]
});

