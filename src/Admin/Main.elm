module Admin.Main exposing (main)

import Accessibility exposing (Html, a, div, h1, img, nav, text)
import Admin.Data.Registration as Registration exposing (Registration)
import Admin.Pages.Detail as Detail
import Admin.Pages.Home as Home
import Admin.Pages.UpdateBasket as UpdateBasket
import Admin.Pages.UpdateInfo as UpdateInfo
import Admin.Pages.UpdateWorkshops as UpdateWorkshops
import Admin.Route as Route exposing (Route)
import Browser exposing (Document)
import Browser.Navigation as Navigation
import Html
import Html.Attributes exposing (class, src, style)
import Html.Events as Events
import Url exposing (Url)



{- MAIN -}


main : Program () Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = always Sub.none
        , onUrlChange = ChangedUrl
        , onUrlRequest = ClickedLink
        }


init : () -> Url.Url -> Navigation.Key -> ( Model, Cmd Msg )
init flags url key =
    setRoute (Route.fromUrl url) key



{- MODEL -}


type Page
    = Home Home.Model
    | Detail Detail.Model
    | UpdateInfo UpdateInfo.Model
    | UpdateWorkshops UpdateWorkshops.Model
    | UpdateBasket UpdateBasket.Model


type alias Model =
    { key : Navigation.Key
    , page : Page
    }



{- VIEW -}


view : Model -> Document Msg
view model =
    let
        viewPage page msg =
            { title = page.title ++ " – Administration Comboros 2019"
            , body =
                [ nav [ class "navbar is-info" ]
                    [ div [ class "navbar-brand" ]
                        [ div [ class "navbar-item" ]
                            [ a [ Route.href Route.Home ]
                                [ img
                                    "Comboros"
                                    [ src "https://www.comboros.com/assets/images/interface/logo-blanc.png" ]
                                ]
                            ]
                        ]
                    ]
                , h1
                    [ class "title is-size-2 has-text-centered"
                    , style "margin-top" "1rem"
                    , style "margin-bottom" "0"
                    ]
                    [ text page.title ]
                , Html.map msg page.body
                ]
            }
    in
    case model.page of
        Home home ->
            viewPage (Home.view home) GotHomeMsg

        Detail detail ->
            viewPage (Detail.view detail) GotDetailMsg

        UpdateInfo updateInfo ->
            viewPage (UpdateInfo.view updateInfo) GotUpdateInfoMsg

        UpdateWorkshops updateWorkshops ->
            viewPage (UpdateWorkshops.view updateWorkshops) GotUpdateWorkshopsMsg

        UpdateBasket updateBasket ->
            viewPage (UpdateBasket.view updateBasket) GotUpdateBasketMsg



{- UPDATE -}


type Msg
    = ChangedUrl Url
    | ClickedLink Browser.UrlRequest
    | GotHomeMsg Home.Msg
    | GotDetailMsg Detail.Msg
    | GotUpdateInfoMsg UpdateInfo.Msg
    | GotUpdateWorkshopsMsg UpdateWorkshops.Msg
    | GotUpdateBasketMsg UpdateBasket.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case ( msg, model.page ) of
        ( ClickedLink urlRequest, _ ) ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Navigation.pushUrl model.key (Url.toString url) )

                Browser.External url ->
                    ( model, Navigation.load url )

        ( ChangedUrl url, _ ) ->
            setRoute (Route.fromUrl url) model.key

        ( GotHomeMsg subMsg, Home subModel ) ->
            let
                ( pageModel, cmd ) =
                    Home.update subMsg subModel
            in
            ( { model | page = Home pageModel }, Cmd.map GotHomeMsg cmd )

        ( GotDetailMsg subMsg, Detail subModel ) ->
            let
                ( pageModel, cmd ) =
                    Detail.update subMsg subModel
            in
            ( { model | page = Detail pageModel }, Cmd.map GotDetailMsg cmd )

        ( GotUpdateInfoMsg subMsg, UpdateInfo subModel ) ->
            let
                ( pageModel, cmd ) =
                    UpdateInfo.update subMsg subModel
            in
            ( { model | page = UpdateInfo pageModel }, Cmd.map GotUpdateInfoMsg cmd )

        ( GotUpdateWorkshopsMsg subMsg, UpdateWorkshops subModel ) ->
            let
                ( pageModel, cmd ) =
                    UpdateWorkshops.update subMsg subModel
            in
            ( { model | page = UpdateWorkshops pageModel }, Cmd.map GotUpdateWorkshopsMsg cmd )

        ( GotUpdateBasketMsg subMsg, UpdateBasket subModel ) ->
            let
                ( pageModel, cmd ) =
                    UpdateBasket.update subMsg subModel
            in
            ( { model | page = UpdateBasket pageModel }, Cmd.map GotUpdateBasketMsg cmd )

        ( _, _ ) ->
            ( model, Cmd.none )


setRoute : Maybe Route -> Navigation.Key -> ( Model, Cmd Msg )
setRoute route key =
    case route of
        Nothing ->
            goHome key Home.init

        -- TODO: nice 404 screen
        Just Route.Home ->
            goHome key Home.init

        Just (Route.Detail id) ->
            goDetail key (Detail.init id key)

        Just (Route.UpdateInfo id) ->
            goUpdateInfo key (UpdateInfo.init id key)

        Just (Route.UpdateWorkshops id) ->
            goUpdateWorkshops key (UpdateWorkshops.init id key)

        Just (Route.UpdateBasket id) ->
            goUpdateBasket key (UpdateBasket.init id key)


goHome : Navigation.Key -> ( Home.Model, Cmd Home.Msg ) -> ( Model, Cmd Msg )
goHome key ( home, cmd ) =
    ( { key = key, page = Home home }
    , Cmd.map GotHomeMsg cmd
    )


goDetail : Navigation.Key -> ( Detail.Model, Cmd Detail.Msg ) -> ( Model, Cmd Msg )
goDetail key ( detail, cmd ) =
    ( { key = key, page = Detail detail }
    , Cmd.map GotDetailMsg cmd
    )


goUpdateInfo : Navigation.Key -> ( UpdateInfo.Model, Cmd UpdateInfo.Msg ) -> ( Model, Cmd Msg )
goUpdateInfo key ( updateInfo, cmd ) =
    ( { key = key, page = UpdateInfo updateInfo }
    , Cmd.map GotUpdateInfoMsg cmd
    )


goUpdateWorkshops : Navigation.Key -> ( UpdateWorkshops.Model, Cmd UpdateWorkshops.Msg ) -> ( Model, Cmd Msg )
goUpdateWorkshops key ( updateWorkshops, cmd ) =
    ( { key = key, page = UpdateWorkshops updateWorkshops }
    , Cmd.map GotUpdateWorkshopsMsg cmd
    )


goUpdateBasket : Navigation.Key -> ( UpdateBasket.Model, Cmd UpdateBasket.Msg ) -> ( Model, Cmd Msg )
goUpdateBasket key ( updateBasket, cmd ) =
    ( { key = key, page = UpdateBasket updateBasket }
    , Cmd.map GotUpdateBasketMsg cmd
    )
