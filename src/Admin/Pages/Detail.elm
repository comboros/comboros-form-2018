module Admin.Pages.Detail exposing (Model, Msg, init, update, view)

import Accessibility exposing (Html, a, article, blockquote, br, button, div, h3, h4, p, section, span, table, tbody, td, text, th, thead, tr)
import Accessibility.Widget
import Admin.Data.Basket exposing (Ticket)
import Admin.Data.Registration as Registration exposing (Address, PaymentType(..), PriceType(..), Registration, RegistrationState(..))
import Admin.Route as Route
import Browser.Navigation as Navigation
import Common.Day exposing (Day(..))
import Common.Moment exposing (Moment(..))
import Common.View exposing (spinner, viewFetchError, viewRegistrationState)
import Common.Workshop exposing (Workshop)
import Html.Attributes exposing (class, disabled, href, style)
import Html.Events exposing (onClick)
import Http
import Task
import Time



{- MODEL -}


type alias Model =
    { id : Int
    , key : Navigation.Key
    , status : Status
    , zone : Time.Zone
    }


type Status
    = Loading
    | LoadingFailed Http.Error
    | Loaded Registration
    | Validating Registration
    | ValidatingFailed Registration Http.Error
    | SendingInvoice Registration
    | SendingInvoiceFailed Registration Http.Error
    | ShowingDeletionModal Registration
    | Deleting Registration
    | DeletingFailed Registration Http.Error


init : Int -> Navigation.Key -> ( Model, Cmd Msg )
init id key =
    ( { id = id, key = key, status = Loading, zone = Time.utc }
    , Cmd.batch
        [ Registration.fetch id |> Http.send CompletedRegistrationLoad
        , Task.perform GotTimeZone Time.here
        ]
    )



{- VIEW -}


view : Model -> { title : String, body : Html Msg }
view model =
    case model.status of
        Loading ->
            { title = "Chargement"
            , body =
                section [ class "section" ]
                    [ spinner
                    , viewBackButton
                    ]
            }

        LoadingFailed e ->
            { title = "Problème"
            , body =
                section [ class "section" ]
                    [ viewFetchError e
                    , viewBackButton
                    ]
            }

        Loaded registration ->
            { title = "Détail de l’inscription #" ++ String.fromInt registration.id
            , body =
                section [ class "section" ] <|
                    viewContent registration
                        ++ [ viewBackButton
                           , viewControls { state = registration.state, disabled = False, lastBilling = registration.lastBilling, zone = model.zone }
                           ]
            }

        Validating registration ->
            { title = "Détail de l’inscription #" ++ String.fromInt registration.id
            , body =
                section [ class "section" ] <|
                    viewContent registration
                        ++ [ viewBackButton
                           , viewControls { state = registration.state, disabled = True, lastBilling = registration.lastBilling, zone = model.zone }
                           ]
            }

        ValidatingFailed registration error ->
            { title = "Détail de l’inscription #" ++ String.fromInt registration.id
            , body =
                section [ class "section" ] <|
                    viewFetchError error
                        :: viewContent registration
                        ++ [ viewBackButton
                           , viewControls { state = registration.state, disabled = False, lastBilling = registration.lastBilling, zone = model.zone }
                           ]
            }

        SendingInvoice registration ->
            { title = "Détail de l’inscription #" ++ String.fromInt registration.id
            , body =
                section [ class "section" ] <|
                    viewContent registration
                        ++ [ viewBackButton
                           , viewControls { state = registration.state, disabled = True, lastBilling = registration.lastBilling, zone = model.zone }
                           ]
            }

        SendingInvoiceFailed registration error ->
            { title = "Détail de l’inscription #" ++ String.fromInt registration.id
            , body =
                section [ class "section" ] <|
                    viewFetchError error
                        :: viewContent registration
                        ++ [ viewBackButton
                           , viewControls { state = registration.state, disabled = False, lastBilling = registration.lastBilling, zone = model.zone }
                           ]
            }

        ShowingDeletionModal registration ->
            { title = "Détail de l’inscription #" ++ String.fromInt registration.id
            , body =
                section [ class "section" ] <|
                    viewContent registration
                        ++ [ viewDeletionModal
                           , viewBackButton
                           , viewControls { state = registration.state, disabled = True, lastBilling = registration.lastBilling, zone = model.zone }
                           ]
            }

        Deleting registration ->
            { title = "Détail de l’inscription #" ++ String.fromInt registration.id
            , body =
                section [ class "section" ] <|
                    viewContent registration
                        ++ [ viewBackButton
                           , viewControls { state = registration.state, disabled = True, lastBilling = registration.lastBilling, zone = model.zone }
                           ]
            }

        DeletingFailed registration error ->
            { title = "Détail de l’inscription #" ++ String.fromInt registration.id
            , body =
                section [ class "section" ] <|
                    viewFetchError error
                        :: viewContent registration
                        ++ [ viewBackButton
                           , viewControls { state = registration.state, disabled = False, lastBilling = registration.lastBilling, zone = model.zone }
                           ]
            }


viewContent : Registration -> List (Html Msg)
viewContent registration =
    [ h3 [ class "title is-3" ]
        [ text (registration.surname ++ " " ++ registration.name)
        , viewRegistrationState registration.state
        ]
    , div [ class "columns" ]
        [ viewInfos registration
        , viewBasket registration
        , viewWorkshops registration
        ]
    ]


viewDeletionModal : Html Msg
viewDeletionModal =
    div [ class "modal is-active" ]
        [ div [ class "modal-background" ] []
        , div [ class "modal-content notification" ]
            [ p []
                [ text "Toute suppression est "
                , span [ class "has-text-weight-bold" ] [ text "définitive" ]
                , text ". "
                , text "Êtes-vous absolument certain-e de vouloir supprimer cette inscription ?"
                ]
            , div [ class "buttons is-right" ]
                [ button [ class "button is-text", onClick ClickedCloseModalButton ]
                    [ text "Annuler" ]
                , button [ class "button is-danger is-pulled-right", onClick SubmittedDeletion ]
                    [ text "Confirmer" ]
                ]
            ]
        , button
            [ class "modal-close is-large"
            , Accessibility.Widget.label "close"
            , onClick ClickedCloseModalButton
            ]
            []
        ]


viewControls : { state : RegistrationState, disabled : Bool, lastBilling : Time.Posix, zone : Time.Zone } -> Html Msg
viewControls spec =
    div [ class "buttons is-right" ]
        [ button
            [ class "button is-danger is-outlined"
            , disabled (spec.state == Paid || spec.disabled)
            , onClick ClickedDeletionButton
            ]
            [ text "Supprimer" ]
        , button
            [ class "button is-primary is-outlined"
            , disabled (spec.state == Inconsistent || spec.disabled)
            , onClick SubmittedInvoiceMailing
            ]
            [ text "Renvoyer la facture", viewBillingDate spec.zone spec.lastBilling ]
        , button
            [ class "button is-info"
            , disabled (spec.state /= Unpaid || spec.disabled)
            , onClick SubmittedValidation
            ]
            [ text "Valider le paiement" ]
        ]


viewBillingDate : Time.Zone -> Time.Posix -> Html msg
viewBillingDate zone time =
    let
        stringify =
            String.fromInt >> String.padLeft 2 '0'

        date =
            String.fromInt (Time.toDay zone time)
                ++ " "
                ++ toFrenchMonth (Time.toMonth zone time)
                ++ " "
                ++ String.fromInt (Time.toYear zone time)
                ++ " à "
                ++ stringify (Time.toHour zone time)
                ++ "H"
                ++ stringify (Time.toMinute zone time)
    in
    span
        [ class "tag"
        , style "position" "absolute"
        , style "top" "3.5em"
        , style "z-index" "1"
        ]
        [ text <| "Dernière facturation le " ++ date ]


toFrenchMonth : Time.Month -> String
toFrenchMonth month =
    case month of
        Time.Jan ->
            "Janvier"

        Time.Feb ->
            "Février"

        Time.Mar ->
            "Mars"

        Time.Apr ->
            "Avril"

        Time.May ->
            "Mai"

        Time.Jun ->
            "Juin"

        Time.Jul ->
            "Juillet"

        Time.Aug ->
            "Août"

        Time.Sep ->
            "Septembre"

        Time.Oct ->
            "Octobre"

        Time.Nov ->
            "Novembre"

        Time.Dec ->
            "Décembre"


viewBackButton : Html msg
viewBackButton =
    a [ class "button is-light is-pulled-left", Route.href Route.Home ]
        [ text "Retour" ]


viewInfos : Registration -> Html Msg
viewInfos registration =
    article [ class "column" ]
        [ h4
            [ class "title is-4" ]
            [ text "Infos"
            , viewUpdateButton (Route.UpdateInfo registration.id)
            ]
        , simpleTable
            [ ( "Paie par", viewPaymentType registration.paymentType )
            , ( "Adresse", viewAddress registration.address )
            , ( "Courriel", viewEmail registration.email )
            , ( "Tél.", text registration.phone )
            , ( "Majorité révolue"
              , if registration.responsible == Nothing then
                    text "Oui"

                else
                    text "Non"
              )
            , ( "Adulte resp."
              , registration.responsible
                    |> Maybe.withDefault ""
                    |> text
              )
            ]
        ]


viewBasket : Registration -> Html msg
viewBasket registration =
    let
        priceLabel =
            case registration.priceType of
                Full ->
                    [ text "Prix" ]

                Reduced ->
                    [ text "Prix ", span [ class "has-text-grey" ] [ text "(réduit)" ] ]
    in
    article [ class "column" ]
        [ h4
            [ class "title is-4" ]
            [ text "Panier"
            , viewUpdateButton (Route.UpdateBasket registration.id)
            ]
        , table [ class "table is-fullwidth is-hoverable" ]
            [ thead []
                [ th [] [ text "Intitulé" ]
                , th [] [ text "Quantité" ]
                , th [] priceLabel
                ]
            , tbody []
                (List.map (viewTicketRow registration.priceType) registration.basket
                    ++ [ viewTotalRow registration.priceType registration.basket ]
                )
            ]
        ]


viewTicketRow : PriceType -> Ticket -> Html msg
viewTicketRow priceType ticket =
    let
        getter =
            case priceType of
                Full ->
                    .price

                Reduced ->
                    .reducedPrice
    in
    tr []
        [ td [] [ text ticket.name ]
        , td [] [ text (String.fromInt ticket.quantity) ]
        , td [] [ text (String.fromFloat (getter ticket) ++ " €") ]
        ]


viewTotalRow : PriceType -> List Ticket -> Html msg
viewTotalRow priceType basket =
    let
        getter =
            case priceType of
                Full ->
                    .price

                Reduced ->
                    .reducedPrice

        total =
            Admin.Data.Basket.getTotal basket getter
    in
    tr []
        [ th [] [ text "Total" ]
        , td [] []
        , td [] [ text (String.fromFloat total ++ " €") ]
        ]


viewWorkshops : Registration -> Html msg
viewWorkshops registration =
    article [ class "column" ]
        [ h4 [ class "title is-4" ]
            [ text "Ateliers"
            , viewUpdateButton (Route.UpdateWorkshops registration.id)
            ]
        , table [ class "table is-fullwidth is-hoverable" ]
            [ thead []
                [ th [] [ text "Créneau" ]
                , th [] [ text "Intitulé" ]
                , th [] [ text "Groupe" ]
                ]
            , tbody [] <|
                List.map
                    viewWorkshopRow
                    registration.workshops
            ]
        ]


viewWorkshopRow : Workshop -> Html msg
viewWorkshopRow workshop =
    tr []
        [ td [] [ viewWorkshopSlot workshop ]
        , td [] [ text workshop.name ]
        , td [] [ text workshop.band ]
        ]


viewWorkshopSlot : Workshop -> Html msg
viewWorkshopSlot workshop =
    let
        day =
            case workshop.day of
                Saturday ->
                    "Samedi"

                Sunday ->
                    "Dimanche"

                Monday ->
                    "Lundi"

                Tuesday ->
                    "Mardi"

                Wednesday ->
                    "Mercredi"

                Thursday ->
                    "Jeudi"

                Friday ->
                    "Vendredi"

        moment =
            case workshop.moment of
                Morning ->
                    "matin"

                Afternoon ->
                    "après-midi"
    in
    text (day ++ " " ++ moment)


viewPaymentType : PaymentType -> Html msg
viewPaymentType paymentType =
    case paymentType of
        Check ->
            text "Chèque"

        Transfer ->
            text "Virement"


viewAddress : Address -> Html msg
viewAddress addr =
    blockquote [] <|
        [ text addr.line1
        , br []
        ]
            ++ (Maybe.map (\str -> [ text str, br [] ]) addr.line2
                    |> Maybe.withDefault []
               )
            ++ [ text (addr.postalCode ++ " " ++ addr.city)
               , br []
               , text addr.country
               ]


viewEmail : String -> Html msg
viewEmail email =
    a [ href ("mailto:" ++ email) ] [ text email ]


simpleTable : List ( String, Html msg ) -> Html msg
simpleTable rows =
    let
        toRow =
            \( key, val ) ->
                tr []
                    [ th [ class "has-text-right" ] [ text key ]
                    , td [] [ val ]
                    ]
    in
    table [ class "table is-fullwidth is-hoverable" ]
        [ tbody [] (List.map toRow rows) ]


viewUpdateButton : Route.Route -> Html msg
viewUpdateButton route =
    a
        [ class "button is-small is-text is-pulled-right has-text-weight-normal"
        , Route.href route
        ]
        [ text "Modifier" ]



{- UPDATE -}


type Msg
    = SubmittedValidation
    | SubmittedInvoiceMailing
    | SubmittedDeletion
    | ClickedDeletionButton
    | ClickedCloseModalButton
    | CompletedRegistrationLoad (Result Http.Error Registration)
    | CompletedValidationLoad (Result Http.Error String)
    | CompletedInvoiceMailingLoad (Result Http.Error String)
    | CompletedDeletionLoad (Result Http.Error String)
    | GotTimeZone Time.Zone
    | GotTimeNow Time.Posix


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SubmittedValidation ->
            case model.status of
                Loaded registration ->
                    ( { model | status = Validating registration }
                    , Registration.validate registration.id
                        |> Http.send CompletedValidationLoad
                    )

                _ ->
                    ( model, Cmd.none )

        SubmittedInvoiceMailing ->
            case model.status of
                Loaded registration ->
                    ( { model | status = SendingInvoice registration }
                    , Registration.sendInvoice registration.id
                        |> Http.send CompletedInvoiceMailingLoad
                    )

                _ ->
                    ( model, Cmd.none )

        SubmittedDeletion ->
            case model.status of
                ShowingDeletionModal registration ->
                    ( { model | status = Deleting registration }
                    , Registration.delete registration.id
                        |> Http.send CompletedDeletionLoad
                    )

                _ ->
                    ( model, Cmd.none )

        ClickedDeletionButton ->
            case model.status of
                Loaded registration ->
                    ( { model | status = ShowingDeletionModal registration }
                    , Cmd.none
                    )

                _ ->
                    ( model, Cmd.none )

        ClickedCloseModalButton ->
            case model.status of
                ShowingDeletionModal registration ->
                    ( { model | status = Loaded registration }
                    , Cmd.none
                    )

                _ ->
                    ( model, Cmd.none )

        CompletedRegistrationLoad (Ok registration) ->
            ( { model | status = Loaded registration }, Cmd.none )

        CompletedRegistrationLoad (Err error) ->
            ( { model | status = LoadingFailed error }, Cmd.none )

        CompletedValidationLoad (Ok _) ->
            case model.status of
                Validating registration ->
                    let
                        newRegistration =
                            { registration | state = Paid }
                    in
                    ( { model | status = Loaded newRegistration }, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        CompletedValidationLoad (Err error) ->
            case model.status of
                Validating registration ->
                    ( { model | status = ValidatingFailed registration error }
                    , Cmd.none
                    )

                _ ->
                    ( model, Cmd.none )

        CompletedInvoiceMailingLoad (Ok _) ->
            case model.status of
                SendingInvoice registration ->
                    {- Don’t modify the status to `Loaded` just yet: we want to
                       retrieve Date.now() before.
                    -}
                    ( model, Task.perform GotTimeNow Time.now )

                _ ->
                    ( model, Cmd.none )

        CompletedInvoiceMailingLoad (Err error) ->
            case model.status of
                SendingInvoice registration ->
                    ( { model | status = SendingInvoiceFailed registration error }, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        CompletedDeletionLoad (Ok _) ->
            case model.status of
                Deleting registration ->
                    ( { model | status = Loading }
                    , Route.replaceUrl model.key Route.Home
                    )

                _ ->
                    ( model, Cmd.none )

        CompletedDeletionLoad (Err error) ->
            case model.status of
                Deleting registration ->
                    ( { model | status = DeletingFailed registration error }
                    , Cmd.none
                    )

                _ ->
                    ( model, Cmd.none )

        GotTimeZone zone ->
            ( { model | zone = zone }, Cmd.none )

        GotTimeNow time ->
            case model.status of
                SendingInvoice registration ->
                    let
                        newRegistration =
                            { registration | lastBilling = time }
                    in
                    ( { model | status = Loaded newRegistration }, Cmd.none )

                _ ->
                    ( model, Cmd.none )
