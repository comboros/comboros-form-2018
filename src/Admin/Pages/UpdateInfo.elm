module Admin.Pages.UpdateInfo exposing (Model, Msg, init, update, view)

import Accessibility exposing (Html, a, article, button, div, em, fieldset, footer, form, h3, header, inputText, label, legend, p, section, span, text)
import Accessibility.Key exposing (tabbable)
import Accessibility.Widget exposing (pressed)
import Admin.Data.Registration as Registration exposing (PaymentType(..), PersonalInfo, PriceType(..), Registration, RegistrationState(..), updatePersonalInfo)
import Admin.Route as Route
import AssocList as Dict exposing (Dict)
import Browser
import Browser.Navigation as Navigation
import Common.View exposing (spinner, viewFetchError, viewInputText)
import Html exposing (input)
import Html.Attributes exposing (action, checked, class, classList, disabled, for, href, id, name, style, type_)
import Html.Events exposing (onClick, onInput, onSubmit)
import Http
import Url
import Validate



{- MODEL -}


type alias Model =
    { id : Int
    , key : Navigation.Key
    , status : Status
    }


type Status
    = Loading
    | LoadingFailed Http.Error
    | Editing Registration Errors PersonalInfo
    | Saving
    | SavingFailed Http.Error


type alias Errors =
    Dict Field String


init : Int -> Navigation.Key -> ( Model, Cmd Msg )
init id key =
    ( { id = id, key = key, status = Loading }
    , fetchRegistration id
    )


initForm : Registration -> PersonalInfo
initForm registration =
    -- Simply initialise the form from the retrieved registration
    { surname = registration.surname
    , name = registration.name
    , responsible = registration.responsible
    , addressLine1 = registration.address.line1
    , addressLine2 = registration.address.line2
    , postalCode = registration.address.postalCode
    , city = registration.address.city
    , country = registration.address.country
    , phone = registration.phone
    , email = registration.email
    , priceType = registration.priceType
    , paymentType = registration.paymentType
    , state = registration.state
    }



{- VIEW -}


view : Model -> { title : String, body : Html Msg }
view model =
    case model.status of
        Loading ->
            { title = "Chargement"
            , body =
                section [ class "section" ]
                    [ spinner
                    , viewBackButton model.id
                    ]
            }

        LoadingFailed error ->
            { title = "Problème lors du chargement"
            , body =
                section [ class "section" ]
                    [ viewFetchError error
                    , viewBackButton model.id
                    ]
            }

        Editing registration errors form ->
            { title = "Édition de l’inscription #" ++ String.fromInt model.id
            , body =
                section [ class "section" ]
                    [ Html.form [ name "personal-info", onSubmit SubmittedForm ]
                        [ div [ class "columns" ]
                            [ article [ class "column" ] <| viewInfos errors form
                            , article [ class "column" ] <| viewContact errors form
                            , article [ class "column" ] <| viewAddress errors form
                            ]
                        , article [ style "margin-top" "1.5rem", style "margin-bottom" "1.5rem" ] <|
                            viewPayment form
                        , div [ class "field" ]
                            [ div [ class "control" ]
                                [ viewBackButton model.id
                                , viewSubmitButton
                                ]
                            ]
                        ]
                    ]
            }

        Saving ->
            { title = "Sauvegarde en cours"
            , body =
                section [ class "section" ]
                    [ spinner
                    , viewBackButton model.id
                    ]
            }

        SavingFailed error ->
            { title = "Problème lors de la sauvegarde des modifications"
            , body =
                section [ class "section" ]
                    [ viewFetchError error
                    , viewBackButton model.id
                    ]
            }


viewBackButton : Int -> Html msg
viewBackButton id =
    a [ class "button is-light", Route.href (Route.Detail id) ]
        [ text "Retour" ]


viewSubmitButton : Html msg
viewSubmitButton =
    button [ type_ "submit", class "button is-info is-pulled-right" ]
        [ text "Valider" ]


viewInfos : Errors -> PersonalInfo -> List (Html Msg)
viewInfos errors form =
    [ h3 [ class "is-size-3" ] [ text "État civil" ]
    , viewInputText
        { label = "Prénom"
        , id = "input-text-surname"
        , value = form.surname
        , message = EnteredSurname
        , error = Dict.get Surname errors
        , help = Nothing
        , mandatory = True
        }
    , viewInputText
        { label = "Nom"
        , id = "input-text-name"
        , value = form.name
        , message = EnteredName
        , error = Dict.get Name errors
        , help = Nothing
        , mandatory = True
        }
    , viewInputText
        { label = "Adulte responsable"
        , id = "input-text-responsible"
        , value = Maybe.withDefault "" form.responsible
        , message = EnteredResponsible
        , error = Dict.get Responsible errors
        , help = Just "Vide si la personne est majeure."
        , mandatory = False
        }
    ]


inputClass : Maybe String -> String
inputClass =
    Maybe.map (always "input is-danger") >> Maybe.withDefault "input"


viewAddress : Errors -> PersonalInfo -> List (Html Msg)
viewAddress errors form =
    [ h3 [ class "is-size-3" ] [ text "Adresse" ]
    , viewInputText
        { label = "Principal"
        , id = "input-text-address-line-1"
        , value = form.addressLine1
        , message = EnteredLine1
        , error = Dict.get Line1 errors
        , help = Nothing
        , mandatory = True
        }
    , viewInputText
        { label = "Complément"
        , id = "input-text-address-line-2"
        , value = Maybe.withDefault "" form.addressLine2
        , message = EnteredLine2
        , error = Dict.get Line2 errors
        , help = Nothing
        , mandatory = False
        }
    , viewInputText
        { label = "Code postal"
        , id = "input-text-postal-code"
        , value = form.postalCode
        , message = EnteredPostalCode
        , error = Dict.get PostalCode errors
        , help = Nothing
        , mandatory = True
        }
    , viewInputText
        { label = "Ville"
        , id = "input-text-city"
        , value = form.city
        , message = EnteredCity
        , error = Dict.get City errors
        , help = Nothing
        , mandatory = True
        }
    , viewInputText
        { label = "Pays"
        , id = "input-text-country"
        , value = form.country
        , message = EnteredCountry
        , error = Dict.get Country errors
        , help = Nothing
        , mandatory = True
        }
    ]


viewContact : Errors -> PersonalInfo -> List (Html Msg)
viewContact errors form =
    [ h3 [ class "is-size-3" ] [ text "Contact" ]
    , viewInputText
        { label = "Téléphone"
        , id = "input-text-phone"
        , value = form.phone
        , message = EnteredPhone
        , error = Dict.get Phone errors
        , help = Nothing
        , mandatory = True
        }
    , viewInputText
        { label = "Courriel"
        , id = "input-text-email"
        , value = form.email
        , message = EnteredEmail
        , error = Dict.get Email errors
        , help = Nothing
        , mandatory = True
        }
    ]


viewPayment : PersonalInfo -> List (Html Msg)
viewPayment form =
    [ h3 [ class "is-size-3" ] [ text "Paiement" ]
    , fieldset [ class "field" ]
        [ legend [ class "label" ] [ text "Typologie de prix" ]
        , viewRadioButtons "input-radio-price-type"
            [ { label = "Plein", checked = form.priceType == Full, onCheck = ChosePriceType Full }
            , { label = "Reduit", checked = form.priceType == Reduced, onCheck = ChosePriceType Reduced }
            ]
        ]
    , fieldset [ class "field" ]
        [ legend [ class "label" ] [ text "Mode de paiement" ]
        , viewRadioButtons "input-type-payment-type"
            [ { label = "Chèque"
              , checked = form.paymentType == Check
              , onCheck = ChosePaymentType Check
              }
            , { label = "Virement"
              , checked = form.paymentType == Transfer
              , onCheck = ChosePaymentType Transfer
              }
            ]
        ]
    , div [ class "columns is-centered" ]
        [ viewStateCard form.state unpaidStateCard
        , viewStateCard form.state paidStateCard
        ]
    ]


type alias StateCard =
    { title : String, description : List (Html Msg), state : RegistrationState }


unpaidStateCard : StateCard
unpaidStateCard =
    { title = "En attente du paiement"
    , description =
        [ p [ style "min-height" "4rem" ]
            [ text "Nous n’avons pas encore reçu le paiement pour cette inscription." ]
        , p [ class "is-size-7", style "min-height" "5rem" ]
            [ text "C’est l’état initial d’une inscription." ]
        ]
    , state = Unpaid
    }


paidStateCard : StateCard
paidStateCard =
    { title = "Payé"
    , description =
        [ p [ style "min-height" "4rem" ]
            [ text "Le paiement a été reçu." ]
        , p [ class "is-size-7", style "min-height" "5rem" ]
            [ text "Si vous sélectionnez le statut "
            , em [] [ text "payé" ]
            , text " directement depuis cet écran, "
            , text "le courriel automatique de confirmation ne sera "
            , em [] [ text "pas" ]
            , text " envoyé."
            ]
        ]
    , state = Paid
    }


viewStateCard : RegistrationState -> StateCard -> Html Msg
viewStateCard currentState spec =
    let
        isSelected =
            spec.state == currentState
    in
    div [ class "column is-one-quarter" ]
        [ div [ classList [ ( "card bundle", True ), ( "selected", isSelected ) ] ]
            [ header [ class "card-header" ]
                [ p [ class "card-header-title is-centered" ] [ text spec.title ] ]
            , div [ class "card-content" ]
                [ div [ class "content has-text-centered" ] spec.description ]
            , footer [ class "card-footer" ]
                [ button
                    [ class "card-footer-item button is-white has-text-link"
                    , type_ "button"
                    , disabled isSelected
                    , pressed (Just isSelected)
                    , tabbable True
                    , onClick (ChoseRegistrationState spec.state)
                    ]
                    [ text "Sélectionner" ]
                ]
            ]
        ]


viewRadioButtons : String -> List { label : String, checked : Bool, onCheck : msg } -> Html msg
viewRadioButtons fieldName specList =
    let
        specToHtml =
            \spec ->
                label [ class "radio" ]
                    [ input
                        [ type_ "radio"
                        , name fieldName
                        , checked spec.checked
                        , onInput (always spec.onCheck)
                        ]
                        []
                    , text spec.label
                    ]
    in
    div [ class "control" ] <|
        List.map specToHtml specList



{- UPDATE -}


type Msg
    = NoOp
    | EnteredSurname String
    | EnteredName String
    | EnteredResponsible String
    | EnteredLine1 String
    | EnteredLine2 String
    | EnteredPostalCode String
    | EnteredCity String
    | EnteredCountry String
    | EnteredPhone String
    | EnteredEmail String
    | ChosePriceType PriceType
    | ChosePaymentType PaymentType
    | ChoseRegistrationState RegistrationState
    | SubmittedForm
    | CompletedRegistrationLoad (Result Http.Error Registration)
    | CompletedSave (Result Http.Error String)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        EnteredSurname surname ->
            updateForm (\f -> { f | surname = surname }) model

        EnteredName name ->
            updateForm (\f -> { f | name = name }) model

        EnteredResponsible responsible ->
            updateForm (\f -> { f | responsible = stringToMaybe responsible }) model

        EnteredLine1 line1 ->
            updateForm (\f -> { f | addressLine1 = line1 }) model

        EnteredLine2 line2 ->
            updateForm (\f -> { f | addressLine2 = stringToMaybe line2 }) model

        EnteredPostalCode postalCode ->
            updateForm (\f -> { f | postalCode = postalCode }) model

        EnteredCity city ->
            updateForm (\f -> { f | city = city }) model

        EnteredCountry country ->
            updateForm (\f -> { f | country = country }) model

        EnteredPhone phone ->
            updateForm (\f -> { f | phone = phone }) model

        EnteredEmail email ->
            updateForm (\f -> { f | email = email }) model

        ChosePriceType priceType ->
            updateForm (\f -> { f | priceType = priceType }) model

        ChosePaymentType paymentType ->
            updateForm (\f -> { f | paymentType = paymentType }) model

        ChoseRegistrationState state ->
            updateForm (\f -> { f | state = state }) model

        SubmittedForm ->
            case model.status of
                Editing r _ form ->
                    case Validate.validate formValidator form of
                        Ok _ ->
                            ( { model | status = Saving }
                            , saveForm model.id form
                            )

                        Err errorList ->
                            ( { model | status = Editing r (Dict.fromList errorList) form }
                            , Cmd.none
                            )

                _ ->
                    ( model, Cmd.none )

        CompletedRegistrationLoad (Ok registration) ->
            ( { model | status = Editing registration Dict.empty (initForm registration) }
            , Cmd.none
            )

        CompletedRegistrationLoad (Err e) ->
            ( { model | status = LoadingFailed e }, Cmd.none )

        CompletedSave (Ok _) ->
            ( model, Route.replaceUrl model.key (Route.Detail model.id) )

        CompletedSave (Err e) ->
            -- TODO: restore form!
            ( { model | status = SavingFailed e }, Cmd.none )


updateForm : (PersonalInfo -> PersonalInfo) -> Model -> ( Model, Cmd Msg )
updateForm f model =
    case model.status of
        Editing r e form ->
            ( { model | status = Editing r e (f form) }, Cmd.none )

        _ ->
            ( model, Cmd.none )


stringToMaybe : String -> Maybe String
stringToMaybe s =
    case s of
        "" ->
            Nothing

        _ ->
            Just s



{- VALIDATE -}


type Field
    = Surname
    | Name
    | Responsible
    | Line1
    | Line2
    | PostalCode
    | City
    | Country
    | Phone
    | Email
    | PaymentType
    | PriceType
    | State


formValidator : Validate.Validator ( Field, String ) PersonalInfo
formValidator =
    Validate.all
        [ Validate.ifBlank .surname ( Surname, requiredMsg )
        , Validate.ifBlank .name ( Name, requiredMsg )
        , Validate.ifBlank .addressLine1 ( Line1, requiredMsg )
        , Validate.ifBlank .postalCode ( PostalCode, requiredMsg )
        , Validate.ifBlank .city ( City, requiredMsg )
        , Validate.ifBlank .phone ( Phone, requiredMsg )
        , Validate.ifInvalidEmail .email
            (\email -> ( Email, invalidEmailMsg email ))
        ]


requiredMsg : String
requiredMsg =
    "Ce champ est obligatoire"


invalidEmailMsg : String -> String
invalidEmailMsg email =
    case email of
        "" ->
            requiredMsg

        _ ->
            "Il semble compliqué d’envoyer un courriel à `" ++ email ++ "`… "



{- HTTP -}


fetchRegistration : Int -> Cmd Msg
fetchRegistration id =
    Registration.fetch id |> Http.send CompletedRegistrationLoad


saveForm : Int -> PersonalInfo -> Cmd Msg
saveForm id form =
    Registration.updatePersonalInfo id form
        |> Http.send CompletedSave
