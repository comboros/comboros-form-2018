module Admin.Pages.UpdateWorkshops exposing (Model, Msg, init, update, view)

import Accessibility exposing (Html, a, button, div, label, option, section, select, text)
import Admin.Data.Registration as Registration exposing (Registration)
import Admin.Route as Route
import AssocList as Dict exposing (Dict)
import Browser.Navigation as Navigation
import Common.Category exposing (Category(..))
import Common.Day as Day exposing (Day(..))
import Common.Moment as Moment exposing (Moment(..))
import Common.View
import Common.Workshop as Workshop exposing (Workshop)
import Html
import Html.Attributes exposing (class, for, id, name, selected, type_, value)
import Html.Events exposing (onInput, onSubmit)
import Http



{- MODEL -}


type alias Model =
    { id : Int
    , key : Navigation.Key
    , status : Status
    }


type Status
    = Loading
    | LoadingGotRegistration Registration
    | LoadingGotWorkshops (List Workshop)
    | LoadingFailed Http.Error
    | Editing (List Dropdown)
    | Saving
    | SavingFailed Http.Error


type alias Dropdown =
    { head : Workshop
    , tail : List Workshop
    , selection : Maybe Workshop
    }


init : Int -> Navigation.Key -> ( Model, Cmd Msg )
init id key =
    let
        registration =
            Registration.fetch id
                |> Http.send CompletedRegistrationLoad

        workshops =
            Workshop.fetchAll
                |> Http.send CompletedWorkshopsLoad
    in
    ( { id = id
      , key = key
      , status = Loading
      }
    , Cmd.batch [ registration, workshops ]
    )


initDropdowns : Registration -> List Workshop -> List Dropdown
initDropdowns registration workshops =
    List.foldl
        (\workshop dict ->
            let
                selection =
                    List.map .id registration.workshops

                selected =
                    List.member workshop.id selection
            in
            Dict.update ( workshop.day, workshop.moment )
                (\maybe ->
                    case maybe of
                        Just dropdown ->
                            Just
                                { head = workshop
                                , tail = dropdown.head :: dropdown.tail
                                , selection =
                                    if selected then
                                        Just workshop

                                    else
                                        dropdown.selection
                                }

                        Nothing ->
                            Just
                                { head = workshop
                                , tail = []
                                , selection =
                                    if selected then
                                        Just workshop

                                    else
                                        Nothing
                                }
                )
                dict
        )
        Dict.empty
        workshops
        |> Dict.values



{- VIEW -}


view : Model -> { title : String, body : Html Msg }
view model =
    case model.status of
        Editing dropdowns ->
            { title = "Édition des réservations de l’inscription #" ++ String.fromInt model.id
            , body =
                section [ class "section" ]
                    [ Html.form [ name "workshops", onSubmit SubmittedForm ] <|
                        List.map viewDropdown dropdowns
                            ++ [ viewBackButton model.id
                               , viewSubmitButton
                               ]
                    ]
            }

        LoadingFailed error ->
            { title = "Problème lors du chargement"
            , body =
                section [ class "section" ]
                    [ Common.View.viewFetchError error
                    , viewBackButton model.id
                    ]
            }

        SavingFailed error ->
            { title = "Problème lors de la sauvegarde des modifications"
            , body =
                section [ class "section" ]
                    [ Common.View.viewFetchError error
                    , viewBackButton model.id
                    ]
            }

        _ ->
            { title = "Chargement"
            , body =
                section [ class "section" ]
                    [ Common.View.spinner
                    , viewBackButton model.id
                    ]
            }


viewDropdown : Dropdown -> Html Msg
viewDropdown dropdown =
    let
        day =
            Day.toString dropdown.head.day

        moment =
            Moment.toString dropdown.head.moment

        dropdownId =
            "dropdown-" ++ day ++ "-" ++ moment
    in
    div [ class "field" ]
        [ label [ class "label", for dropdownId ]
            [ text <| day ++ " " ++ moment ]
        , div [ class "control" ]
            [ div [ class "select" ]
                [ select [ onInput (WorkshopSelected dropdown), id dropdownId ] <|
                    [ option [ selected (dropdown.selection == Nothing), value "Nothing" ]
                        [ text "" ]
                    ]
                        ++ List.map (viewOption dropdown.selection) (dropdown.head :: dropdown.tail)
                ]
            ]
        ]


viewOption : Maybe Workshop -> Workshop -> Html Msg
viewOption selection workshop =
    option
        [ selected (selection == Just workshop)
        , value (String.fromInt workshop.id)
        ]
        [ text <| workshop.name ++ " – " ++ workshop.band ]


viewBackButton : Int -> Html msg
viewBackButton id =
    a [ class "button is-light", Route.href (Route.Detail id) ]
        [ text "Retour" ]


viewSubmitButton : Html msg
viewSubmitButton =
    button [ type_ "submit", class "button is-info is-pulled-right" ]
        [ text "Valider" ]



{- UPDATE -}


type Msg
    = WorkshopSelected Dropdown String
    | SubmittedForm
    | CompletedRegistrationLoad (Result Http.Error Registration)
    | CompletedWorkshopsLoad (Result Http.Error (List Workshop))
    | CompletedSave (Result Http.Error String)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        CompletedRegistrationLoad (Ok registration) ->
            case model.status of
                Loading ->
                    ( { model | status = LoadingGotRegistration registration }, Cmd.none )

                LoadingGotWorkshops workshops ->
                    ( { model | status = Editing (initDropdowns registration workshops) }, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        CompletedRegistrationLoad (Err error) ->
            ( { model | status = LoadingFailed error }, Cmd.none )

        CompletedWorkshopsLoad (Ok workshops) ->
            case model.status of
                Loading ->
                    ( { model | status = LoadingGotWorkshops workshops }, Cmd.none )

                LoadingGotRegistration registration ->
                    ( { model | status = Editing (initDropdowns registration workshops) }, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        CompletedWorkshopsLoad (Err error) ->
            ( { model | status = LoadingFailed error }, Cmd.none )

        WorkshopSelected dropdown str ->
            case model.status of
                Editing dropdowns ->
                    case String.toInt str of
                        Just id ->
                            ( { model | status = Editing (selectWorkshop dropdowns id) }, Cmd.none )

                        Nothing ->
                            ( { model | status = Editing (unselectWorkshop dropdowns dropdown) }, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        SubmittedForm ->
            case model.status of
                Editing dropdowns ->
                    -- TODO: validate
                    ( { model | status = Saving }
                    , save model.id dropdowns
                    )

                _ ->
                    ( model, Cmd.none )

        CompletedSave (Ok _) ->
            ( model, Route.replaceUrl model.key (Route.Detail model.id) )

        CompletedSave (Err e) ->
            ( { model | status = SavingFailed e }, Cmd.none )


selectWorkshop : List Dropdown -> Int -> List Dropdown
selectWorkshop dropdowns id =
    dropdowns
        |> List.map
            (\dropdown ->
                let
                    workshop =
                        dropdownGetWorkshop dropdown id
                in
                if workshop /= Nothing then
                    { dropdown | selection = workshop }

                else
                    dropdown
            )


dropdownGetWorkshop : Dropdown -> Int -> Maybe Workshop
dropdownGetWorkshop dropdown id =
    List.filter (\w -> w.id == id) (dropdown.head :: dropdown.tail)
        |> List.head


unselectWorkshop : List Dropdown -> Dropdown -> List Dropdown
unselectWorkshop dropdowns dropdown =
    dropdowns
        |> List.map
            (\d ->
                if d == dropdown then
                    { d | selection = Nothing }

                else
                    d
            )



{- FORM -}
{- HTTP -}


save : Int -> List Dropdown -> Cmd Msg
save id dropdowns =
    List.filterMap (\d -> d.selection) dropdowns
        |> List.map .id
        |> Registration.updateBooking id
        |> Http.send CompletedSave
