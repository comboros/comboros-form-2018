module Admin.Data.Basket exposing (Ticket, decoder, getTotal)

import Json.Decode exposing (Decoder, float, int, string, succeed)
import Json.Decode.Pipeline exposing (required)


type alias Ticket =
    { name : String
    , quantity : Int
    , price : Float
    , reducedPrice : Float
    }



{- ENCODE/DECODE -}


decoder : Decoder Ticket
decoder =
    succeed Ticket
        |> required "name" string
        |> required "quantity" int
        |> required "price" float
        |> required "reducedPrice" float



{- HELPERS -}


getTotal : List Ticket -> (Ticket -> Float) -> Float
getTotal basket getter =
    basket
        |> List.map (\t -> toFloat t.quantity * getter t)
        |> List.foldl (+) 0
