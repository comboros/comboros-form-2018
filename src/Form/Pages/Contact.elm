module Form.Pages.Contact exposing (Model, Msg, encode, init, update, validate, view)

import Accessibility as Html exposing (Html, button, div, h3, hr, label, section, text)
import Accessibility.Landmark exposing (main_)
import AssocList as Dict exposing (Dict)
import Browser.Navigation as Navigation
import Form.Route as Route
import Form.Views.Form exposing (inputText)
import Html exposing (form)
import Html.Attributes exposing (action, class, id, name, style, tabindex, type_)
import Html.Events exposing (onCheck, onClick, onInput, onSubmit)
import Json.Encode as Json
import Validate



-- MODEL --


type Field
    = Line1
    | Line2
    | PostalCode
    | City
    | Country
    | Phone
    | Email


type alias Address =
    { line1 : String
    , line2 : String
    , postalCode : String
    , city : String
    , country : String
    }


type alias Model =
    { address : Address
    , phone : String
    , email : String
    , errors : Dict Field String
    }


init : Model
init =
    { address =
        { line1 = ""
        , line2 = ""
        , postalCode = ""
        , city = ""
        , country = ""
        }
    , phone = ""
    , email = ""
    , errors = Dict.empty
    }


encode : Model -> Json.Value
encode model =
    Json.object
        [ ( "address", encodeAddress model.address )
        , ( "phone", Json.string model.phone )
        , ( "email", Json.string model.email )
        ]


encodeAddress : Address -> Json.Value
encodeAddress address =
    Json.object
        [ ( "line1", Json.string address.line1 )
        , ( "line2"
          , if String.isEmpty address.line1 then
                Json.null

            else
                Json.string address.line2
          )
        , ( "postalCode", Json.string address.postalCode )
        , ( "city", Json.string address.city )
        , ( "country", Json.string address.country )
        ]



-- VIEW


viewLine1 : Model -> Html Msg
viewLine1 model =
    inputText
        { label = "Adresse"
        , id = "input-text-address"
        , placeholder = "40, avenue de la République"
        , value = model.address.line1
        , onInput = SetLine1
        , error = Dict.get Line1 model.errors
        , mandatory = True
        }


viewLine2 : Model -> Html Msg
viewLine2 model =
    inputText
        { label = "Complément"
        , id = "input-text-complement"
        , placeholder = "Au fond de la cour"
        , value = model.address.line2
        , onInput = SetLine2
        , error = Dict.get Line2 model.errors
        , mandatory = False
        }


viewPostalCode : Model -> Html Msg
viewPostalCode model =
    inputText
        { label = "Code postal"
        , id = "input-text-postal-code"
        , placeholder = "63200"
        , value = model.address.postalCode
        , onInput = SetPostalCode
        , error = Dict.get PostalCode model.errors
        , mandatory = True
        }


viewCity : Model -> Html Msg
viewCity model =
    inputText
        { label = "Ville"
        , id = "input-text-city"
        , placeholder = "Saint-Bonnet-Près-Riom"
        , value = model.address.city
        , onInput = SetCity
        , error = Dict.get City model.errors
        , mandatory = True
        }


viewCountry : Model -> Html Msg
viewCountry model =
    inputText
        { label = "Pays"
        , id = "input-text-country"
        , placeholder = "France"
        , value = model.address.country
        , onInput = SetCountry
        , error = Dict.get Country model.errors
        , mandatory = True
        }


viewPhone : Model -> Html Msg
viewPhone model =
    inputText
        { label = "Téléphone"
        , id = "input-text-phone"
        , placeholder = "04 73 63 36 75"
        , value = model.phone
        , onInput = SetPhone
        , error = Dict.get Phone model.errors
        , mandatory = True
        }


viewEmail : Model -> Html Msg
viewEmail model =
    inputText
        { label = "Courriel"
        , id = "input-text-email"
        , placeholder = "brayauds@wanadoo.fr"
        , value = model.email
        , onInput = SetEmail
        , error = Dict.get Email model.errors
        , mandatory = True
        }


viewForm : Model -> Html Msg
viewForm model =
    form [ name "contact", onSubmit Next ]
        [ h3 [ class "title" ]
            [ text "Contact" ]
        , viewLine1 model
        , viewLine2 model
        , viewPostalCode model
        , viewCity model
        , viewCountry model
        , hr [] []
        , viewPhone model
        , viewEmail model
        , div [ class "is-clearfix", style "margin-top" "2rem" ]
            [ div [ class "buttons is-pulled-right" ]
                [ button
                    [ type_ "button", class "button is-secondary", onClick Previous ]
                    [ text "Retour" ]
                , button
                    [ type_ "submit", class "button is-primary" ]
                    [ text "Suite" ]
                ]
            ]
        ]


view : Model -> Html Msg
view model =
    section [ class "section", main_, id "main-section", tabindex -1 ]
        [ div [ class "container" ]
            [ viewForm model ]
        ]



-- UPDATE


type Msg
    = Previous
    | Next
    | SetLine1 String
    | SetLine2 String
    | SetPostalCode String
    | SetCity String
    | SetCountry String
    | SetPhone String
    | SetEmail String


update : Navigation.Key -> Msg -> Model -> ( Model, Cmd Msg )
update key msg model =
    case msg of
        Previous ->
            ( model
            , Route.newUrl key Route.PersonalInfo
            )

        Next ->
            case Validate.validate formValidator model of
                Ok _ ->
                    ( model, Route.newUrl key Route.Tickets )

                Err errorList ->
                    ( { model | errors = Dict.fromList errorList }
                    , Cmd.none
                    )

        SetLine1 line1 ->
            let
                address =
                    model.address

                newAddress =
                    { address | line1 = line1 }
            in
            ( { model
                | address =
                    newAddress
              }
            , Cmd.none
            )

        SetLine2 line2 ->
            let
                address =
                    model.address

                newAddress =
                    { address | line2 = line2 }
            in
            ( { model
                | address =
                    newAddress
              }
            , Cmd.none
            )

        SetPostalCode postalCode ->
            let
                address =
                    model.address

                newAddress =
                    { address | postalCode = postalCode }
            in
            ( { model
                | address =
                    newAddress
              }
            , Cmd.none
            )

        SetCity city ->
            let
                address =
                    model.address

                newAddress =
                    { address | city = city }
            in
            ( { model
                | address =
                    newAddress
              }
            , Cmd.none
            )

        SetCountry country ->
            let
                address =
                    model.address

                newAddress =
                    { address | country = country }
            in
            ( { model
                | address =
                    newAddress
              }
            , Cmd.none
            )

        SetPhone phone ->
            ( { model | phone = phone }
            , Cmd.none
            )

        SetEmail email ->
            ( { model | email = email }
            , Cmd.none
            )



-- VALIDATE --


validate : Model -> Dict Field String
validate model =
    case Validate.validate formValidator model of
        Ok _ ->
            Dict.empty

        Err errorList ->
            Dict.fromList errorList


addressValidator : Validate.Validator ( Field, String ) Model
addressValidator =
    Validate.all
        [ Validate.ifBlank (.address >> .line1) ( Line1, requiredMsg )
        , Validate.ifBlank (.address >> .postalCode) ( PostalCode, requiredMsg )
        , Validate.ifBlank (.address >> .city) ( City, requiredMsg )
        , Validate.ifBlank (.address >> .country) ( Country, requiredMsg )
        ]


formValidator : Validate.Validator ( Field, String ) Model
formValidator =
    Validate.all
        [ addressValidator
        , Validate.ifBlank .phone ( Phone, requiredMsg )
        , Validate.ifInvalidEmail .email
            (\email -> ( Email, invalidEmailMsg email ))
        ]


requiredMsg : String
requiredMsg =
    "Ce champ est obligatoire"


invalidEmailMsg : String -> String
invalidEmailMsg email =
    case email of
        "" ->
            requiredMsg

        _ ->
            "Il semble compliqué d’envoyer un courriel à `" ++ email ++ "`… "
