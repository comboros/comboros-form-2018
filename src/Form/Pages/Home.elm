module Form.Pages.Home exposing (Msg, update, view)

import Accessibility as Html exposing (Html, a, button, div, h1, h2, h3, p, section, strong, text)
import Accessibility.Landmark exposing (contentInfo, main_)
import Browser.Navigation as Navigation
import Form.Route as Route
import Html.Attributes exposing (class, href, id, style, tabindex, type_, value)
import Html.Events exposing (onClick)



-- MODEL... none! --
-- VIEW --


view : Html Msg
view =
    section [ class "section", main_, id "main-section", tabindex -1 ]
        [ div [ class "container" ]
            (formInstructions
                ++ workshopInstructions
                ++ paymentInstructions
            )
        , div [ class "has-text-centered" ]
            [ button
                [ type_ "button"
                , class "button is-primary"
                , onClick Next
                ]
                [ text "Remplir le formulaire" ]
            ]
        , div [ class "notification", style "margin-top" "1.5rem", contentInfo ]
            dataTreatmentWarning
        ]


formInstructions : List (Html msg)
formInstructions =
    [ h3 [ class "title is-3" ]
        [ text "Formulaire" ]
    , div [ class "content" ]
        [ p []
            [ text "Ce formulaire vous offre la possibilité de réserver "
            , strong [ class "has-text-danger" ]
                [ text "des forfaits pour toute la durée du festival" ]
            , text ", et "
            , strong [ class "has-text-danger" ]
                [ text "des places dans les ateliers" ]
            , text ". Les forfaits à la journée et places à l’unité pourront être achetés sur place, à la biletterie."
            ]
        , p []
            [ text "L’inscription à comboros est "
            , strong [] [ text "nominative" ]
            , text ". Cela signifie que vous devrez effectuer "
            , strong [ class "has-text-danger" ]
                [ text "une inscription par personne" ]
            , text " (et une seule !)."
            ]
        ]
    ]


workshopInstructions : List (Html msg)
workshopInstructions =
    [ h3 [ class "title is-3" ]
        [ text "Ateliers" ]
    , div [ class "content" ]
        [ p []
            [ text
                ("Les stages sont répartis sur quatre jours, du jeudi après-midi au dimanche soir. "
                    ++ "Chacun dure trois heures, tant en danse qu’en musique. "
                    ++ "Le nombre maximum de stagiaires pour un stage dépend du lieu où il se tient "
                    ++ "et de la limite donnée par l’intervenant-e, "
                    ++ "et peut donc varier d’un stage à l’autre. "
                )
            , strong [ class "has-text-danger" ]
                [ text "Il ne sera pas possible de changer de stage en cours d’atelier. " ]
            ]
        , p []
            [ text "La plupart des stages n’ont lieu qu’une seule fois dans le festival. " ]
        ]
    ]


paymentInstructions : List (Html msg)
paymentInstructions =
    [ h3 [ class "title is-3" ]
        [ text "Paiement" ]
    , div [ class "content" ]
        [ p []
            [ text
                ("Une fois le formulaire rempli, "
                    ++ "vous serez invité-e à nous faire parvenir votre paiement par virement ou chèque postal. "
                )
            , strong [ class "has-text-danger" ]
                [ text "La réservation ne sera validée qu’à réception du paiement. " ]
            ]
        , p []
            [ text
                ("Il vous est possible d’annuler sans frais votre inscription jusqu’à 7 jours avant le début du festival. "
                    ++ "Au dela de cette limite, "
                    ++ "l’association conservera 50% du montant versé à titre d’indemnisation."
                )
            ]
        ]
    ]


dataTreatmentWarning : List (Html msg)
dataTreatmentWarning =
    [ h3 [ class "title is-6" ]
        [ text "Avertissement relatif au traitement des données" ]
    , div [ class "content" ]
        [ p [ class "is-size-7" ]
            [ text """Ce modèle sera ultérieurement complété par l’information
concernant les directives relatives au sort des données à caractère personnel
après la mort (article 32-I-6° de la loi du 6 janvier 1978 modifiée).""" ]
        , p [ class "is-size-7" ]
            [ text """Les informations recueillies sur ce formulaire sont
enregistrées dans un fichier informatisé par """
            , a [ href "http://avis-situation-sirene.insee.fr/IdentificationEntrToEtab.action?form.siren=328099270&form.nic=00014" ]
                [ text "Les Brayauds-CDMDT63" ]
            , text """ pour effectuer les opérations relatives à l'inscription
au festival "Comboros" et, éventuellement, au paiement en ligne des frais
d'inscription ; ainsi que la gestion des demandes de droit d'accès,
de rectification et d'opposition."""
            ]
        , p [ class "is-size-7" ]
            [ text """Elles sont conservées pendant 3 ans et sont destinées au
Secrétariat de l'association Les Brayauds-CDMDT63."""
            ]
        , p [ class "is-size-7" ]
            [ text """Conformément à la loi « informatique et libertés »,
vous pouvez exercer votre droit d'accès aux données vous concernant et les
faire rectifier en contactant le """
            , a [ href "brayauds@wanadoo.fr" ]
                [ text "Secrétariat de l'association Les Brayauds-CDMDT63" ]
            , text ", 40 rue de la République, 63200 Saint-Bonnet près Riom"
            ]
        , p [ class "is-size-7" ]
            [ text """Nous vous informons de l’existence de la liste
d'opposition au démarchage téléphonique « Bloctel »,
sur laquelle vous pouvez vous inscrire """
            , a [ href "https://conso.bloctel.fr/" ]
                [ text "ici" ]
            , text "."
            ]
        ]
    ]



-- UPDATE --


type Msg
    = Next


update : Navigation.Key -> Msg -> Cmd Msg
update key msg =
    case msg of
        Next ->
            Route.newUrl key Route.PersonalInfo
