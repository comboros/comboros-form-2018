module Form.Pages.TicketSelectionModal exposing (ExternalMsg(..), Model, Msg(..), init, isActive, update, view)

import Accessibility exposing (Html, button, div, h3, label, p, text)
import Accessibility.Aria exposing (labelledBy)
import Accessibility.Key exposing (enter, escape, onKeyDown)
import Accessibility.Role exposing (dialog, document)
import Accessibility.Widget exposing (hidden, modal)
import Form.Data.Catalog as Catalog exposing (Ticket)
import Html exposing (form, input)
import Html.Attributes exposing (action, class, classList, disabled, for, id, style, type_, value)
import Html.Events exposing (onClick, onInput, onSubmit)



-- MODEL --


type alias Model =
    Maybe
        { ticket : Ticket
        , count : Int
        , isReduced : Bool
        }


init : Model
init =
    Nothing


isActive : Model -> Bool
isActive model =
    model /= Nothing



-- VIEW --


view : Model -> Html Msg
view model =
    let
        ticket =
            Maybe.map .ticket model
                |> Maybe.withDefault Catalog.emptyTicket

        count =
            Maybe.map .count model
                |> Maybe.withDefault 0

        price =
            ticket.price

        total =
            toFloat count * price

        feeMessage =
            Maybe.map .isReduced model
                |> Maybe.map
                    (\x ->
                        if x then
                            "Tarif réduit"

                        else
                            "Tarif plein"
                    )
                |> Maybe.withDefault ""
    in
    div
        [ class "modal"
        , classList [ ( "is-active", isActive model ) ]
        ]
        [ div [ class "modal-background" ] []
        , Html.div
            [ class "modal-content"
            , dialog
            , modal (isActive model)
            , labelledBy "modal-title"
            , hidden (isActive model)
            , onKeyDown [ escape Hide, enter Submit ]
            ]
            [ form [ class "box", onSubmit Submit ]
                [ h3 [ class "title", id "modal-title" ]
                    [ text "Combien en voulez-vous ?" ]
                , div [ class "columns" ]
                    [ div [ class "field column" ]
                        [ label [ class "label", for "input-text-quantity" ]
                            [ text "Quantité" ]
                        , div [ class "control" ]
                            [ input
                                [ class "input"
                                , type_ "number"
                                , value (String.fromInt count)
                                , onInput (String.toInt >> SetCount)
                                , id "input-text-quantity"
                                ]
                                []
                            ]
                        ]
                    , div [ class "column is-narrow is-hidden-mobile" ]
                        [ text "×" ]
                    , div [ class "field column" ]
                        [ label [ class "label", for "input-text-price" ]
                            [ text "Prix" ]
                        , div [ class "control" ]
                            [ input
                                [ class "input"
                                , type_ "text"
                                , value (String.fromFloat price)
                                , disabled True
                                , id "input-text-price"
                                ]
                                []
                            ]
                        ]
                    , div [ class "column is-narrow is-hidden-mobile" ]
                        [ text "=" ]
                    , div [ class "field column" ]
                        [ label [ class "label", for "input-text-total" ]
                            [ text "Total" ]
                        , div [ class "control" ]
                            [ input
                                [ class "input"
                                , type_ "text"
                                , value (String.fromFloat total)
                                , disabled True
                                , id "input-text-total"
                                ]
                                []
                            ]
                        ]
                    ]
                , p [ class "field has-text-centered has-text-grey" ]
                    [ text feeMessage ]
                , div [ class "field is-grouped" ]
                    [ div [ class "control", style "margin-left" "auto" ]
                        [ button
                            [ class "button is-text"
                            , type_ "button"
                            , onClick Hide
                            ]
                            [ text "Annuler" ]
                        ]
                    , div [ class "control" ]
                        [ button [ class "button is-primary", type_ "submit" ]
                            [ text "Valider" ]
                        ]
                    ]
                ]
            ]
        ]



-- UPDATE --


type Msg
    = SetCount (Maybe Int)
    | Hide
    | Submit


type ExternalMsg
    = NoOp
    | Validate Ticket Int
    | Cancel Ticket


update : Msg -> Model -> ( Model, ExternalMsg )
update msg model =
    case msg of
        SetCount quantity ->
            case model of
                Just modal ->
                    ( Just { modal | count = max 0 (Maybe.withDefault 0 quantity) }, NoOp )

                _ ->
                    ( Nothing, NoOp )

        Hide ->
            case model of
                Just modal ->
                    ( Nothing, Cancel modal.ticket )

                Nothing ->
                    ( Nothing, NoOp )

        Submit ->
            case model of
                Just modal ->
                    ( Nothing, Validate modal.ticket modal.count )

                _ ->
                    ( Nothing, NoOp )
