module Common.MaybeSelectList exposing (MaybeSelectList, after, before, empty, fromList, length, select, selected, toList, unselect)

{-| A `MaybeSelectList` is a list which maybe has one element selected |
-}


type MaybeSelectList a
    = MaybeSelectList (List a) (Maybe a) (List a)


empty : MaybeSelectList a
empty =
    MaybeSelectList [] Nothing []


before : MaybeSelectList a -> List a
before (MaybeSelectList beforeSel _ _) =
    beforeSel


after : MaybeSelectList a -> List a
after (MaybeSelectList _ _ afterSel) =
    afterSel


selected : MaybeSelectList a -> Maybe a
selected (MaybeSelectList _ sel _) =
    sel


fromList : List a -> MaybeSelectList a
fromList lst =
    MaybeSelectList lst Nothing []


length : MaybeSelectList a -> Int
length (MaybeSelectList beforeSel maybeSel afterSel) =
    List.length beforeSel
        + (case maybeSel of
            Nothing ->
                0

            Just _ ->
                1
          )
        + List.length afterSel


toList : MaybeSelectList a -> List a
toList (MaybeSelectList beforeSel maybeSel afterSel) =
    case maybeSel of
        Nothing ->
            beforeSel ++ afterSel

        Just sel ->
            beforeSel ++ sel :: afterSel


select : a -> MaybeSelectList a -> MaybeSelectList a
select elt ((MaybeSelectList beforeSel maybeSel afterSel) as original) =
    case maybeSel of
        Nothing ->
            let
                lst =
                    toList original
            in
            if List.member elt lst then
                MaybeSelectList
                    (takeBefore elt lst)
                    (Just elt)
                    (takeAfter elt lst)

            else
                original

        Just sel ->
            if elt == sel then
                original

            else if List.member elt beforeSel then
                let
                    newBefore =
                        takeBefore elt beforeSel

                    newAfter =
                        takeAfter elt beforeSel
                            ++ sel
                            :: afterSel
                in
                MaybeSelectList newBefore (Just elt) newAfter

            else if List.member elt afterSel then
                let
                    newBefore =
                        beforeSel
                            ++ sel
                            :: takeBefore elt afterSel

                    newAfter =
                        takeAfter elt afterSel
                in
                MaybeSelectList newBefore (Just elt) newAfter

            else
                original


unselect : MaybeSelectList a -> MaybeSelectList a
unselect ((MaybeSelectList beforeSel sel afterSel) as original) =
    case sel of
        Nothing ->
            original

        Just elt ->
            MaybeSelectList (beforeSel ++ elt :: afterSel) Nothing []



-- TODO : replace with list-extra takeWhile and dropWhile?


position : a -> List a -> Maybe Int
position elt lst =
    positionHelp elt lst 0


positionHelp : a -> List a -> Int -> Maybe Int
positionHelp elt lst i =
    case lst of
        [] ->
            Nothing

        hd :: tl ->
            if elt == hd then
                Just i

            else
                positionHelp elt tl (i + 1)


takeBefore : a -> List a -> List a
takeBefore elt list =
    case position elt list of
        Nothing ->
            list

        Just i ->
            List.take i list


takeAfter : a -> List a -> List a
takeAfter elt list =
    case position elt list of
        Nothing ->
            []

        Just i ->
            List.drop (i + 1) list
