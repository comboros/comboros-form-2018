module Common.Basket exposing (Basket(..), BundleChoice, UnitaryChoice, build, changeQuantity, countWorkshops, maxWorkshops, selectBundle)

import Admin.Data.Basket
import Common.Ticket as Ticket exposing (Ticket, TicketDTO)


type Basket
    = Basket BundleChoice UnitaryChoice


type alias BundleChoice =
    ( List Ticket, Ticket )


type alias UnitaryChoice =
    List ( Ticket, Int )


selectBundle : Ticket -> Basket -> Basket
selectBundle ticket (Basket ( catalog, _ ) unitary) =
    Basket ( catalog, ticket ) unitary


changeQuantity : Ticket -> Int -> Basket -> Basket
changeQuantity ticket quantity (Basket bundle unitary) =
    Basket bundle
        (List.map
            (\( t, q ) ->
                if t == ticket then
                    ( t, quantity )

                else
                    ( t, q )
            )
            unitary
        )


countWorkshops : Basket -> Int
countWorkshops (Basket ( _, bundle ) unitaries) =
    ( bundle, 1 )
        :: unitaries
        |> List.map (\( t, q ) -> t.workshops * q)
        |> List.foldl (+) 0



{- TODO: get that from a global config -}


maxWorkshops : Int
maxWorkshops =
    7


build : List Admin.Data.Basket.Ticket -> List TicketDTO -> Result String Basket
build basket catalog =
    case List.partition (\dto -> dto.span == "unitary") catalog of
        ( dtoUnitaries, dtoBundles ) ->
            let
                selection =
                    List.map .name basket

                bundles =
                    List.map Ticket.fromDTO dtoBundles

                bundleChoice =
                    List.filter (\t -> List.member t.name selection) bundles
                        |> List.head
                        |> Maybe.map (\t -> ( bundles, t ))
                        |> Result.fromMaybe "No bundle selected"

                unitaryChoice =
                    List.map Ticket.fromDTO dtoUnitaries
                        |> List.map
                            (\t ->
                                List.filter (\u -> t.name == u.name) basket
                                    |> List.head
                                    |> Maybe.map (\u -> ( t, u.quantity ))
                                    |> Maybe.withDefault ( t, 0 )
                            )
            in
            Result.map (\bc -> Basket bc unitaryChoice) bundleChoice
