module Common.Workshop exposing (Workshop, decoder, fetchAll)

import Common.Category as Category exposing (Category(..))
import Common.Day as Day exposing (Day(..))
import Common.Moment as Moment exposing (Moment(..))
import Common.Url as Url
import Http exposing (Request)
import Json.Decode as Decode exposing (Decoder, field, int, list, string, succeed)
import Json.Decode.Pipeline exposing (custom, required)


type alias Workshop =
    { id : Int
    , name : String
    , band : String
    , category : Category
    , day : Day
    , moment : Moment
    , remainder : Int
    }



{- ENCODE/DECODE -}


decoder : Decoder Workshop
decoder =
    succeed Workshop
        |> required "id" int
        |> required "name" string
        |> required "band" string
        |> custom (field "category" Category.decoder)
        |> custom (field "day" Day.decoder)
        |> custom (field "moment" Moment.decoder)
        |> required "remainder" int



{- HTTP -}


fetchAll : Request (List Workshop)
fetchAll =
    Http.get Url.workshops (list decoder)
