<?php

require_once 'inc/config.php';
require_once 'inc/model.php';
require_once 'inc/repository.php';
require_once 'inc/utils.php';

$register = Config::db_prefix . 'register';
$basket = Config::db_prefix . 'basket';
$catalog = Config::db_prefix . 'catalog';

$request = <<<SQL
SELECT
    r.id,
    CONCAT(r.surname, ' ', r.name),
    r.responsible,
    CONCAT(r.line1, ' ', r.line2, ' ', r.postal_code, ' ', r.city, ' ', r.country),
    r.phone,
    r.email,
    r.payment_type,
    r.price_type,
    r.state,
    catalog.name,
    basket.quantity,
    catalog.price,
    catalog.reduced_price
FROM $register r
    LEFT JOIN $basket basket ON basket.registration = r.id
    LEFT JOIN $catalog catalog ON catalog.id = basket.ticket
GROUP BY r.id
SQL;

$headers = [
    'Num.',
    'Nom',
    'Adulte responsable',
    'Adresse',
    'Téléphone',
    'Courriel',
    'Mode de paiement',
    'Tarif',
    'Statut',
    'Forfait',
    'Quantité',
    'Tarif plein',
    'Tarif réduit'
];

$mysqli = connect();
$statement = $mysqli->prepare($request);
if (!$statement) {
    err("Échec lors de la préparation de la requête d’export csv");
}
$ret = $statement->execute();
if (!$ret) {
    err("Échec lors de la récupération des inscriptions");
}
$result = $statement->get_result();

header('Content-Type: text/csv');

$out = fopen('php://output', 'w');
fputcsv($out, $headers);
while ($row = $result->fetch_row()) {
    fputcsv($out, $row);
}
fclose($out);

