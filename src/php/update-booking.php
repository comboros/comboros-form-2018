<?php
/*
 * Update the registration’s workshop list
 */
require_once 'inc/config.php';
require_once 'inc/model.php';
require_once 'inc/repository.php';
require_once 'inc/utils.php';

// receive

if (!isset($_GET['id']) || !($id = $_GET['id'])) {
    err('id query parameter is mandatory', 400);
}

$json = file_get_contents('php://input');
$list = json_decode($json);

// validate

if (!$list) {
    error_log('Received invalid JSON');
    err('Invalid JSON: ' . json_last_error_msg(), 400);
}

$mysqli = connect();

$repository = new RegistrationRepository($mysqli);
$registration = $repository->findOne($id);
if (!$registration) {
    error_log("Received unknown registration id $id");
    err("Registration $id not found", 404);
}

$ticketRepository = new TicketRepository($mysqli);
$oldCount = $ticketRepository->countWorkshops($id);
$newCount = count($list);
if ($oldCount != $newCount) {
    error_log("Received wrong number of workshops ($newCount instead of $oldCount) for registration $id");
    err("Invalid number of workshops $newCount ; Registration $id must have exactly $oldCount workshops", 400);
}

// persist

$repository->updateBooking($id, $list);
if ($registration->state === 'inconsistent') {
    $repository->changeState($id, 'unpaid');
}

http_response_code(204);
exit();

