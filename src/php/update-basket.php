<?php
/*
 * Update the registration’s basket
 */
require_once 'inc/config.php';
require_once 'inc/model.php';
require_once 'inc/repository.php';
require_once 'inc/utils.php';

// receive

if (!isset($_GET['id']) || !($id = $_GET['id'])) {
    err('id query parameter is mandatory', 400);
}

$json = file_get_contents('php://input');
$list = json_decode($json);
$basket = array_filter($list, function($entry) {
    return $entry->quantity > 0;
});

// validate: don’t care to add validation at this point.

// persist

$mysqli = connect();

$ticketRepository = new TicketRepository($mysqli);
$oldCount = $ticketRepository->countWorkshops($id);

$repository = new RegistrationRepository($mysqli);
$repository->updateBasket($id, $basket);

$newCount = $ticketRepository->countWorkshops($id);

if ($oldCount != $newCount) {
    $repository->changeState($id, 'inconsistent');
}

http_response_code(204);
exit();
