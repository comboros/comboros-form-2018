<?php
/*
 * Configuration variables
 */

class Config {
    /*
     * Database access
     */
    const db_url = 'localhost';
    const db_user = 'comboros';
    const db_passwd = 'comboros';
    const db_name = 'comboros';
    const db_charset = 'utf8';

    /*
     * Prefix for database tables (for instance if it’s a shared database
     */
    const db_prefix = 'ct_';
}

