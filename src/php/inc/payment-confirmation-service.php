<?php

require_once 'model.php';

class PaymentConfirmationService {
    /**
     * Send a confirmation email to inform the user payment’s been received.
     */
    public function sendConfirmation($to) {
        $subject = 'Comboros 2019 -- Confirmation de votre inscription';
        $from = 'no-reply@comboros.com';
        $body = $this->mailBody();
        return mail($to, $subject, $body, "From: $from");
    }

    private function mailBody() {
        return <<<BODY
Bonjour,

suite à la réception de votre paiement, nous avons le plaisir de vous confirmer votre inscription à l’édition 2019 de Comboros !

Si vous avez des questions, n’hésitez pas à consulter notre site internet https://www.comboros.com
En cas de problème, il vous est possible de contacter le secrétariat des Brayauds au 04 73 63 36 75 (ouvert aux horaires de bureau).

Merci, et à cet été !
-- 
L’équipe de Comboros.
BODY;
    }
}
