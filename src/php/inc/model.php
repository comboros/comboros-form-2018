<?php

class Registration {
  // Civil state
  public $name;
  public $surname;
  public $responsible;
  // Address
  public $city;
  public $country;
  public $line1;
  public $line2;
  public $postalCode;
  // Contact
  public $email;
  public $phone;

  public $basket;
  public $workshops;
  public $priceType; // reduced fee or normal
  public $paymentType;
  public $state;

  public static function fromJsonObject($obj) {
    self::validate($obj);
    $info = $obj->{'personal-info'};
    $contact = $obj->contact;
    $address = $contact->address;

    $result = new self();
    // Civil state
    $result->name = $info->name;
    $result->surname = $info->surname;
    $result->responsible = $info->responsible;
    // Address
    $result->city = $address->city;
    $result->country = $address->country;
    $result->line1 = $address->line1;
    $result->line2 = $address->line2;
    $result->postalCode = $address->postalCode;
    // Contact
    $result->email = $contact->email;
    $result->phone = $contact->phone;

    $result->basket = $obj->basket;
    $result->workshops = $obj->workshops;
    $result->paymentType = $obj->{'payment-mode'};
    $result->priceType = $obj->{'price-type'};
    $result->state = 'unpaid';
    return $result;
  }

  public static function fromArray($array) {
      $result = new self();
      $result->id = $array['id'];
      $result->priceType = $array['price_type'];
      $result->paymentType = $array['payment_type'];
      $result->state = $array['state'];
      $result->surname = $array['surname'];
      $result->name = $array['name'];
      $result->responsible = $array['responsible'];
      $result->line1 = $array['line1'];
      $result->line2 = $array['line2'];
      $result->postalCode = $array['postal_code'];
      $result->city = $array['city'];
      $result->country = $array['country'];
      $result->phone = $array['phone'];
      $result->email = $array['email'];
      $result->lastBilling = $array['last_billing'];
      return $result;
  }

  private static function validate($obj) {
    if (!$obj) throw new Exception('null');
    if (!isset($obj->{'personal-info'})) {
      throw new Exception('personal-info is mandatory');
    }
    if (!isset($obj->contact)) {
      throw new Exception('contact is mandatory');
    }
    if (!isset($obj->contact->address)) {
      throw new Exception('address is mandatory');
    }
    if (!isset($obj->basket)) {
      throw new Exception('basket is mandatory');
    }
    if (!isset($obj->workshops)) {
      throw new Exception('workshops is mandatory');
    }
    if (!isset($obj->{'payment-mode'})) {
      throw new Exception('payment-mode is mandatory');
    }
  }
}

class Workshop {
    public $id;
    public $name;
    public $band;
    public $category;
    public $day;
    public $moment;

    /**
     * Number of tickets left for this workshop.
     */
    public $remainder;

    public static function fromArray($array) {
        $result = new self();
        $result->id = $array['id'];
        $result->name = $array['name'];
        $result->band = $array['band'];
        $result->category = $array['category'];
        $result->day = $array['day'];
        $result->moment = $array['moment'];
        $result->remainder = $array['remainder'];
        return $result;
    }
}

class Ticket {
    public $id;
    public $name;
    public $description;
    public $price;
    public $reducedPrice;
    public $span;
    public $workshops;

    public static function fromArray($array) {
        $result = new self();
        $result->id = $array['id'];
        $result->name = $array['name'];
        $result->description = $array['description'];
        $result->price = (float) $array['price'];
        $result->reducedPrice = (float) $array['reduced_price'];
        $result->span = $array['span'];
        $result->workshops = $array['workshops'];
        return $result;
    }
}

class BasketDTO {
    public $quantity;
    public $name;
    public $price;
    public $reducedPrice;

    public static function fromArray($array) {
        $result = new self();
        $result->quantity = $array['quantity'];
        $result->name = $array['name'];
        $result->price = (float) $array['price'];
        $result->reducedPrice = (float) $array['reduced_price'];
        return $result;
    }
}

class PersonalInfoDTO {
    public $name;
    public $surname;
    public $responsible;
    public $addressLine1;
    public $addressLine2;
    public $postalCode;
    public $city;
    public $country;
    public $phone;
    public $email;


    public static function fromJsonObject($obj) {
        self::validate($obj);
        $result = new self();
        $result->name = $obj->name;
        $result->surname = $obj->surname;
        $result->responsible = $obj->responsible;
        $result->city = $obj->city;
        $result->country = $obj->country;
        $result->addressLine1 = $obj->addressLine1;
        $result->addressLine2 = $obj->addressLine2;
        $result->postalCode = $obj->postalCode;
        $result->city = $obj->city;
        $result->country = $obj->country;
        $result->phone = $obj->phone;
        $result->email = $obj->email;
        $result->paymentType = $obj->paymentType;
        $result->priceType = $obj->priceType;
        $result->state = $obj->state;
        return $result;
    }

    private static function validate($obj) {
        if (!$obj) throw new Exception('null');
        if (!$obj->name) throw new Exception('name is mandatory');
        if (!$obj->surname) throw new Exception('surname is mandatory');
        if (!$obj->addressLine1) throw new Exception('addressLine1 is mandatory');
        if (!$obj->postalCode) throw new Exception('postalCode is mandatory');
        if (!$obj->city) throw new Exception('city is mandatory');
        if (!$obj->country) throw new Exception('country is mandatory');
        if (!$obj->phone) throw new Exception('phone is mandatory');
        if (!$obj->email) throw new Exception('email is mandatory');
        if (!$obj->paymentType) throw new Exception('paymentType is mandatory');
        if (!$obj->priceType) throw new Exception('priceType is mandatory');
        if (!$obj->state) throw new Exception('state is mandatory');
    }
}
