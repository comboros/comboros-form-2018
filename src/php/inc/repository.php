<?php

require_once "inc/config.php";

class Repository {
    protected $mysqli;

    public function __construct(mysqli $mysqli) {
        $this->mysqli = $mysqli;
    }

    protected function handle_error($msg) {
        $errno = $this->mysqli->errno;
        $error = $this->mysqli->error;
        throw new Exception($msg . " : $errno -- $error");
        return null;
    }
}

class RegistrationRepository extends Repository {

    public function findOne($id) {
        $table = Config::db_prefix . 'register';
        $request = <<<SQL
SELECT
  id,
  price_type,
  payment_type,
  state,
  surname,
  name,
  responsible,
  line1,
  line2,
  postal_code,
  city,
  country,
  phone,
  email,
  UNIX_TIMESTAMP(last_billing) AS `last_billing`
FROM $table
WHERE id = ?
SQL;
        $statement = $this->mysqli->prepare($request);
        if (!$statement) {
            return $this->handle_error('Échec lors de la préparation de la requête de récupération d’une inscription');
        }
        $ret = $statement->bind_param('i', $id);
        if (!$ret) {
            return $this->handle_error('Échec lors de l’attribution des paramètres de la requête de récupération d’une inscription');
        }
        $ret = $statement->execute();
        if (!$ret) {
            return $this->handle_error('Échec lors de la récupération d’une inscription');
        }
        $result = $statement->get_result();
        $row = $result->fetch_assoc();
        if ($row) {
            return Registration::fromArray($row);
        }
        return $row;
    }

    public function suggest($term) {
        $table = Config::db_prefix . 'register';
        $request = <<<SQL
SELECT
  id,
  price_type,
  payment_type,
  state,
  surname,
  name,
  responsible,
  line1,
  line2,
  postal_code,
  city,
  country,
  phone,
  email,
  UNIX_TIMESTAMP(last_billing) AS `last_billing`
FROM $table
WHERE name LIKE ?
    OR surname LIKE ?
    OR id = ?
    OR id = SUBSTRING(?, 2);
SQL;
        $statement = $this->mysqli->prepare($request);
        if (!$statement) {
            return $this->handle_error('Échec lors de la préparation de la requête de suggestion d’une inscription');
        }
        $searchTerm = "$term%";
        $ret = $statement->bind_param('ssss', $searchTerm, $searchTerm, $term, $term);
        if (!$ret) {
            return $this->handle_error('Échec lors de l’attribution des paramètres de la requête de suggestion d’une inscription');
        }
        $ret = $statement->execute();
        if (!$ret) {
            return $this->handle_error('Échec lors de la suggestion d’une inscription');
        }
        $registrations = array();
        foreach($statement->get_result() as $index => $row) {
            $registrations[] = Registration::fromArray($row);
        }
        return $registrations;
    }

    /* resume a registration in a simple association array */
    public function getSynopsis($id) {
        $register = Config::db_prefix . 'register';
        $basket = Config::db_prefix . 'basket';
        $booking = Config::db_prefix . 'booking';
        $request = <<<SQL
SELECT
  $register.id,
  $register.name,
  $register.surname,
  GROUP_CONCAT(
    DISTINCT CONCAT(
        '(',
        $basket.ticket,
        ',',
        $basket.quantity,
        ')'
    )
    SEPARATOR ';'
  ) AS tickets,
  GROUP_CONCAT(
    DISTINCT $booking.workshop SEPARATOR ','
  ) AS workshops
FROM $register
    LEFT JOIN $booking ON $booking.registration = $register.id
    LEFT JOIN $basket ON $basket.registration = $register.id
WHERE $register.id = ?
GROUP BY $register.id;
SQL;
        $statement = $this->mysqli->prepare($request);
        if (!$statement) {
            return $this->handle_error("Échec lors de la préparation de la requête de résumé de l’inscription $id");
        }

        $ret = $statement->bind_param('i', $id);
        if (!$ret) {
            return $this->handle_error("Échec lors de lors de l’attribution des paramètres de la requête de résumé de l’inscription $id");
        }
        $ret = $statement->execute();
        if (!$ret) {
            return $this->handle_error("Échec lors de l’exécution de la requête de résumé de l’inscription $id");
        }
        $result = $statement->get_result();
        $row = $result->fetch_row();
        if (!$row) {
            return $this->handle_error("Échec lors de la récupération du résultat du résumé de l’inscription $id");
        }
        return $row;
    }

    public function save(Registration $registration) {
        $table = Config::db_prefix . 'register';
        $request = <<<SQL
INSERT INTO $table (
  name, surname, responsible,
  line1, line2, city, postal_code, country,
  email, phone,
  payment_type, price_type, state
)
VALUES (
  ?, ?, ?,
  ?, ?, ?, ?, ?,
  ?, ?,
  ?, ?, ?
)
SQL;

        $statement = $this->mysqli->prepare($request);
        if (!$statement) {
            return $this->handle_error("Échec lors de la préparation de la requête d’inscription initiale");
        }

        $ret = $statement->bind_param("sssssssssssss",
            $registration->name,
            $registration->surname,
            $registration->responsible,

            $registration->line1,
            $registration->line2,
            $registration->city,
            $registration->postalCode,
            $registration->country,

            $registration->email,
            $registration->phone,

            $registration->paymentType,
            $registration->priceType,
            $registration->state
        );
        if (!$ret) {
            throw new Exception("Echec lors de l’attribution des paramètres de la requête d’inscription initiale : $statement->errno -- $statement->error");
            return null;
        }

        $ret = $statement->execute();
        if (!$ret) {
            return $this->handle_error("Échec lors de l’enregistrement de l’inscription initiale");
        }

        $this->saveBooking($statement->insert_id, $registration->workshops);
        $this->saveBasket($statement->insert_id, $registration->basket);
        return $statement->insert_id;
    }

    public function changeState($id, $state) {
        $table = Config::db_prefix . 'register';
        $request = "UPDATE $table SET state = ? WHERE id = ?;";
        $statement = $this->mysqli->prepare($request);
        if (!$statement) {
            return $this->handle_error('Échec lors de la préparation de la requête de changement de statut d’une inscription');
        }
        $ret = $statement->bind_param('si', $state, $id);
        if (!$ret) {
            return $this->handle_error('Échec lors de l’attribution des paramètres de la requête de changement de statut d’une inscription');
        }
        $ret = $statement->execute();
        if (!$ret) {
            return $this->handle_error('Échec lors de la requête de changement de statut d’une inscription');
        }
    }

    public function updatePersonalInfo($id, PersonalInfoDTO $dto) {
        if (!$id) {
            throw new Exception('id must not be null');
        }

        $table = Config::db_prefix . 'register';
        $request = <<<SQL
UPDATE $table
SET
  name = ?,
  surname = ?,
  responsible = ?,
  line1 = ?,
  line2 = ?,
  city = ?,
  postal_code = ?,
  country = ?,
  phone = ?,
  email = ?,
  payment_type = ?,
  price_type = ?,
  state = ?
WHERE id = ?
SQL;
        $statement = $this->mysqli->prepare($request);
        if (!$statement) {
            return $this->handle_error("Échec lors de la préparation de la requête de mise à jour de l’inscription $id");
        }

        $ret = $statement->bind_param('sssssssssssssi',
            $dto->name,
            $dto->surname,
            $dto->responsible,
            $dto->addressLine1,
            $dto->addressLine2,
            $dto->city,
            $dto->postalCode,
            $dto->country,
            $dto->phone,
            $dto->email,
            $dto->paymentType,
            $dto->priceType,
            $dto->state,
            $id
        );
        if (!$ret) {
            return $this->handle_error("Échec lors de l’attribution des paramètres de la requête de mise à jour de l’inscription $id");
        }

        $ret = $statement->execute();
        if (!$ret) {
            return $this->handle_error("Échec lors de la mise à jour de l’inscription $id");
        }
    }

    public function updateBooking($id, array $workshops) {
        $this->mysqli->autocommit(false);
        $this->mysqli->begin_transaction();
        try {
            $this->deleteBooking($id);
            $this->saveBooking($id, $workshops);
        } catch (Exception $e) {
            $this->mysqli->rollback();
            throw $e;
        }
        $this->mysqli->commit();
        $this->mysqli->autocommit(true);
    }

    public function updateBasket($id, array $basket) {
        $this->mysqli->autocommit(false);
        $this->mysqli->begin_transaction();
        try {
            $this->deleteBasket($id);
            $this->saveBasket($id, $basket);
        } catch (Exception $e) {
            $this->mysqli->rollback();
            throw $e;
        }
        $this->mysqli->commit();
        $this->mysqli->autocommit(true);
    }

    public function updateLastBilling($id) {
        $table = Config::db_prefix . 'register';
        $request = "UPDATE $table SET last_billing = CURRENT_TIMESTAMP WHERE id = ?";
        $statement = $this->mysqli->prepare($request);
        if (!$statement) {
            return $this->handle_error('Échec lors de la préparation de la requête de mise à jour de la date de dernière facturation');
        }
        $ret = $statement->bind_param('i', $id);
        if (!$ret) {
            return $this->handle_error('Échec lors de l’attribution des paramètres de la requête de mise à jour de la date de dernière facturation');
        }
        $ret = $statement->execute();
        if (!$ret) {
            return $this->handle_error('Échec lors de la mise à jour de la date de dernière facturation');
        }
    }

    public function deleteOne($id) {
        $this->mysqli->autocommit(false);
        $this->mysqli->begin_transaction();
        try {
            $this->deleteBasket($id);
            $this->deleteBooking($id);
            $this->deleteRegistration($id);
        } catch (Exception $e) {
            $this->mysqli->rollback();
            throw $e;
        }
        $this->mysqli->commit();
        $this->mysqli->autocommit(true);
    }

    private function saveBooking($id, array $workshops) {
        $table = Config::db_prefix . 'booking';
        $request = "INSERT INTO $table (registration, workshop) VALUES (?, ?);";
        $statement = $this->mysqli->prepare($request);
        if (!$statement) {
            return $this->handle_error("Échec lors de la préparation de la requête de booking");
        }

        foreach ($workshops as $index => $workshop_id) {
            $ret = $statement->bind_param("ii", $id, $workshop_id);
            if (!$ret) {
                return $this->handle_error("Échec lors de l’attribution des paramètres de la requête de booking");
            }
            $ret = $statement->execute();
            if (!$ret) {
                return $this->handle_error("Échec lors de l’enregistrement d’un booking");
            }
        }
    }

    private function saveBasket($id, array $basket) {
        $table = Config::db_prefix . 'basket';
        $request = "INSERT INTO $table (registration, ticket, quantity) VALUES (?, ?, ?);";
        $statement = $this->mysqli->prepare($request);
        if (!$statement) {
            return $this->handle_error("Échec lors de la préparation de la requête de sauvegarde du panier");
        }

        foreach ($basket as $index => $item) {
            $ticket = $item->ticket;
            $quantity = $item->quantity;
            $ret = $statement->bind_param("iii", $id, $ticket, $quantity);
            if (!$ret) {
                return $this->handle_error("Échec lors de l’attribution des paramètres de la requête de sauvegarde du panier");
            }
            $ret = $statement->execute();
            if (!$ret) {
                return $this->handle_error("Échec lors de l’enregistrement d’un élément du panier");
            }
        }
    }

    private function deleteBooking($id) {
        $table = Config::db_prefix . 'booking';
        $request = "DELETE FROM $table WHERE registration = ?";
        $statement = $this->mysqli->prepare($request);
        if (!$statement) {
            return $this->handle_error('Échec lors de la préparation de la requête de suppression du booking');
        }
        $ret = $statement->bind_param('i', $id);
        if (!$ret) {
            return $this->handle_error('Échec lors de l’attribution des paramètres de la requête de suppression du booking');
        }
        $ret = $statement->execute();
        if (!$ret) {
            return $this->handle_error('Échec lors de la suppression du booking');
        }
    }

    private function deleteBasket($id) {
        $table = Config::db_prefix . 'basket';
        $request = "DELETE FROM $table WHERE registration = ?";
        $statement = $this->mysqli->prepare($request);
        if (!$statement) {
            return $this->handle_error('Échec lors de la préparation de la requête de suppression du panier');
        }
        $ret = $statement->bind_param('i', $id);
        if (!$ret) {
            return $this->handle_error('Échec lors de l’attribution des paramètres de la requête de suppression du panier');
        }
        $ret = $statement->execute();
        if (!$ret) {
            return $this->handle_error('Échec lors de la suppression du panier');
        }
    }

    private function deleteRegistration($id) {
        $table = Config::db_prefix . 'register';
        $request = "DELETE FROM $table WHERE id = ?";
        $statement = $this->mysqli->prepare($request);
        if (!$statement) {
            return $this->handle_error('Échec lors de la préparation de la requête de suppression de l’inscription');
        }
        $ret = $statement->bind_param('i', $id);
        if (!$ret) {
            return $this->handle_error('Échec lors de l’attribution des paramètres de la requête de suppression de l’inscription');
        }
        $ret = $statement->execute();
        if (!$ret) {
            return $this->handle_error('Échec lors de la suppression de l’inscription');
        }
    }
}

class WorkshopRepository extends Repository {
    public function __construct(mysqli $mysqli) {
        $this->mysqli = $mysqli;
    }

    public function findAll() {
        $workshops = Config::db_prefix . 'workshops';
        $booking = Config::db_prefix . 'booking';
        $request = <<<SQL
SELECT id, name, band, category, day, moment, gauge - COUNT(workshop) AS remainder
FROM $workshops
    LEFT JOIN $booking ON workshop = id
GROUP BY id, name, band, category, day, moment, gauge, workshop;
SQL;
        $statement = $this->mysqli->prepare($request);
        if (!$statement) {
            return $this->handle_error("Échec lors de la préparation de la requête de récupération des ateliers");
        }
        $ret = $statement->execute();
        if (!$ret) {
            return $this->handle_error("Échec lors de la récupération des ateliers");
        }

        $workshops = array();
        foreach($statement->get_result() as $index => $row) {
            $workshops[] = Workshop::fromArray($row);
        }
        return $workshops;
    }

    public function findByRegistrationId($id) {
        $workshops = Config::db_prefix . 'workshops';
        $booking = Config::db_prefix . 'booking';
        $request = <<<SQL
SELECT id, name, band, category, day, moment, 0 as remainder
FROM $booking
    JOIN $workshops ON workshop = id
WHERE $booking.registration = ?;
SQL;
        $statement = $this->mysqli->prepare($request);
        if (!$statement) {
            return $this->handle_error("Échec lors de la préparation de la requête de récupération des ateliers de la réservation $id");
        }
        $ret = $statement->bind_param('i', $id);
        if (!$ret) {
            return $this->handle_error("Échec lors de l’attribution des paramètres de la requête de récupération des ateliers de la réservation $id");
        }
        $ret = $statement->execute();
        if (!$ret) {
            return $this->handle_error("Échec lors de la récupération des ateliers de la réservation $id");
        }

        $workshops = array();
        foreach($statement->get_result() as $index => $row) {
            $workshops[] = Workshop::fromArray($row);
        }
        return $workshops;
    }

}

class TicketRepository extends Repository {
    public function __construct(mysqli $mysqli) {
        $this->mysqli = $mysqli;
    }

    public function findAll() {
        $table = Config::db_prefix . 'catalog';
        $request = "SELECT id, name, description, price, reduced_price, span, workshops FROM $table;";
        $statement = $this->mysqli->prepare($request);
        if (!$statement) {
            return $this->handle_error("Échec lors de la préparation de la requête de récupération des tickets");
        }
        $ret = $statement->execute();
        if (!$ret) {
            return $this->handle_error("Échec lors de la récupération des tickets");
        }

        $tickets = array();
        foreach($statement->get_result() as $index => $row) {
            $tickets[] = Ticket::fromArray($row);
        }
        return $tickets;
    }

    public function findByRegistrationId($id) {
        $catalog = Config::db_prefix . 'catalog';
        $basket = Config::db_prefix . 'basket';
        $request = <<<SQL
SELECT basket.quantity, ticket.name, ticket.price, ticket.reduced_price
FROM $basket AS basket
JOIN $catalog AS ticket ON ticket.id = basket.ticket
WHERE basket.registration = ?
ORDER BY ticket.span, ticket.price DESC;
SQL;
        $statement = $this->mysqli->prepare($request);
        if (!$statement) {
            return $this->handle_error("Échec lors de la préparation de la requête de récupération du panier de la réservation $id");
        }
        $ret = $statement->bind_param('i', $id);
        if (!$ret) {
            return $this->handle_error("Échec lors de l’attribution des paramètres de la requête de récupération du panier de la réservation $id");
        }
        $ret = $statement->execute();
        if (!$ret) {
            return $this->handle_error("Échec lors de la récupération du panier de la réservation $id");
        }

        $results = array();
        foreach($statement->get_result() as $index => $row) {
            $results[] = BasketDTO::fromArray($row);
        }
        return $results;
    }

    public function countWorkshops($id) {
        $catalog = Config::db_prefix . 'catalog';
        $basket = Config::db_prefix . 'basket';
        $request = <<<SQL
SELECT SUM(basket.quantity * ticket.workshops)
FROM $basket AS basket
JOIN $catalog AS ticket ON ticket.id = basket.ticket
WHERE basket.registration = ?
SQL;
        $statement = $this->mysqli->prepare($request);
        if (!$statement) {
            return $this->handle_error("Échec lors de la préparation de la requête de récupération du nombre d'ateliers de la réservation $id");
        }
        $ret = $statement->bind_param('i', $id);
        if (!$ret) {
            return $this->handle_error("Échec lors de l’attribution des paramètres de la requête de récupération du nombre d'ateliers de la réservation $id");
        }
        $ret = $statement->execute();
        if (!$ret) {
            return $this->handle_error("Échec lors de la récupération du nombre d'ateliers de la réservation $id");
        }
        $result = $statement->get_result();
        $row = $result->fetch_row();
        if (!$row || !count($row)) {
            return $this->handle_error("Échec lors de la récupération du résultat du nombre d'ateliers de la réservation $id");
        }
        return $row[0];
    }
}

