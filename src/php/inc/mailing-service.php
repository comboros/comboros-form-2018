<?php
require_once 'model.php';
require_once 'repository.php';

class MailingService {

    private $ticketRepository;
    private $workshopRepository;

    public function __construct($ticketRepository, $workshopRepository) {
        $this->ticketRepository = $ticketRepository;
        $this->workshopRepository = $workshopRepository;
    }

    const transferIntroduction = <<<TRANSFER
votre pré-inscription à l’édition 2019 de Comboros a bien été prise en compte !
Elle sera définitive sur réception de votre règlement.

Vous avez opté pour le virement bancaire, voici les coordonnées bancaires des Brayauds :

RIB FRANCE
  Banque 16806
  Guichet 02200
  Numéro de compte 66080092190
  Clé 40

IBAN ÉTRANGER
  FR76 1680 6022 0066 0800 9219 040

BIC
  AGRIFRPP868

TRANSFER;

    const checkIntroduction = <<<CHECK
votre pré-inscription à l’édition 2019 de Comboros a bien été prise en compte !
Elle sera définitive sur réception de votre règlement.

Vous avez opté pour le paiement par chèque à l’ordre de

    Les Brayauds — CDMDT–63

voici l’adresse à laquelle nous l’envoyer :

    Les Brayauds — CDMDT–63
    40, rue de la République
    63200 Saint-Bonnet-Près-Riom

CHECK;

    public function sendConfirmation(Registration $registration, $id) {
        $subject = 'Comboros 2019 -- Confirmation de votre commande';
        $from = 'no-reply@comboros.com';
        $to = $registration->email;
        $introduction;
        switch ($registration->paymentType) {
        case 'transfer':
            $introduction = self::transferIntroduction;
            break;
        case 'check':
            $introduction = self::checkIntroduction;
            break;
        default:
            throw new Exception("Invalid paymentType $registration->paymentType");
        }
        $body = $this->mailBody($introduction, $this->infoTable($registration),
            $this->basketTable($id, $registration->priceType),
            $this->workshopsTable($id));
        return mail($to, $subject, $body, "From: $from");
    }

    private function mailBody($introduction, $infoTable, $basketTable, $workshopsTable) {
        return <<<BODY
Bonjour,

$introduction

Voici un résumé de votre inscription :

Panier
------

$basketTable


Ateliers
--------

$workshopsTable


Informations
----------------

$infoTable


----------------

Si vous avez des questions, n’hésitez pas à consulter notre site internet https://www.comboros.com
En cas de problème, il vous est possible de contacter le secrétariat des Brayauds au 04 73 63 36 75 (ouvert aux horaires de bureau).

Merci et à cet été !
-- 
L’équipe de Comboros.
BODY;
    }


    private function infoTable(Registration $r) {
        $resp = $r->responsible == null ? 'N.A.' : $r->responsible;
        return <<<INFO
Nom : $r->name
Prénom : $r->surname
Responsable : $resp

Adresse :
  $r->line1
  $r->line2
  $r->postalCode $r->city $r->country

Courriel : $r->email
Tél. : $r->phone
INFO;
    }

    private function basketTable($id, $priceType) {
        $basketRows = $this->ticketRepository->findByRegistrationId($id);
        $result = '';
        $sum = 0;
        foreach ($basketRows as $index => $row) {
            $price = $priceType === 'reduced' ? $row->reducedPrice : $row->price;
            $result .= "    $row->quantity × $row->name = $price €\n";
            $sum += $price * $row->quantity;
        }
        $result .= "\nPour un total de $sum €";
        return $result;
    }

    private function workshopsTable($id) {
        $workshops = $this->workshopRepository->findByRegistrationId($id);
        $result = '';
        foreach ($workshops as $index => $row) {
            $slot = self::getSlot($row->day, $row->moment);
            $result .= "    $slot\n        $row->name avec $row->band\n";
        }
        return $result;
    }

    private static function getSlot($day, $moment) {
        $result = '';
        switch ($day) {
        case 'saturday':
            $result .= 'Samedi';
            break;
        case 'sunday':
            $result .= 'Dimanche';
            break;
        case 'monday':
            $result .= 'Lundi';
            break;
        case 'tuesday':
            $result .= 'Mardi';
            break;
        case 'wednesday':
            $result .= 'Mercredi';
            break;
        case 'thursday':
            $result .= 'Jeudi';
            break;
        case 'friday':
            $result .= 'Vendredi';
            break;
        }
        switch ($moment) {
        case 'morning':
            $result .= ' matin';
            break;
        case 'afternoon':
            $result .= ' après-midi';
            break;
        }
        return $result;
    }
}
