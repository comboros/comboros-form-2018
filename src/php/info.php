<?php
/**
 * GET
 *
 * Registration info
 */

require_once 'inc/utils.php';
require_once 'inc/model.php';
require_once 'inc/repository.php';

// Validate input

if (!isset($_GET['id']) || !($id = $_GET['id'])) {
    err('id query parameter is mandatory', 400);
}

// Repositories

$mysqli = connect();
$registrationRepository = new RegistrationRepository($mysqli);
$workshopRepository = new WorkshopRepository($mysqli);
$ticketRepository = new TicketRepository($mysqli);

$registration = $registrationRepository->findOne($id);
if (!$registration) {
    err("unknown registration id $id", 404);
}
$registration->workshops = $workshopRepository->findByRegistrationId($id);
$registration->basket = $ticketRepository->findByRegistrationId($id);

header('Content-Type: application/json');
echo json_encode($registration);
